close all; clear; clc
patient='05';

% function CMT_Data_Prep(patient)
rawFolder=['/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/CMT1A-R21/Scan Development/phantom_' patient '/Raw/'];
scans=dir([rawFolder '*.par']);
body={'thigh','calf'};
addpath('/Users/alisonroth/Documents/Code/MATLAB/vuTools')
addpath('/Users/alisonroth/Documents/Code/MATLAB/Data_Load_Save/')

%% Function
for b=1:2
    savefilename=['/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/CMT1A-R21/Scan Development/phantom_' patient '/Processed/CMT_dev_' patient '_' body{b} '_data.mat'];
    for s=1:size(scans,1)
        if strfind(scans(s).name,body{b})
            if strfind(scans(s).name,'UHR')
                img=vuOpenImage([rawFolder scans(s).name]);
                dxUHR=img.Spc(1);
                dyUHR=img.Spc(2);
                dzUHR=img.Spc(3);
                sUHR=img.Data;
                if exist(savefilename)
                    save(savefilename, 'dxUHR', 'dyUHR', 'dzUHR', 'sUHR','-append');
                else
                    save(savefilename, 'dxUHR', 'dyUHR', 'dzUHR', 'sUHR');
                end
            elseif strfind(scans(s).name,'MTR')
                img=vuOpenImage([rawFolder scans(s).name]);
                dxMT=img.Spc(1);
                dyMT=img.Spc(2);
                dzMT=img.Spc(3);
                MTTR=img.Parms.repetition_time;
                sMTON=img.Data(:,:,:,:,2); 
                sMTOFF=img.Data(:,:,:,:,1);
                
                img=loadParRec([rawFolder scans(s).name]);
                MTOFFTEs=img.imgdef.echo_time.uniq';
                MTFA=img.imgdef.image_flip_angle_in_degrees.uniq;
                if exist(savefilename)
                    save(savefilename, 'dxMT', 'dyMT', 'dzMT', 'sMTON', 'sMTOFF', 'MTOFFTEs','MTTR','MTFA','-append');
                else
                    save(savefilename, 'dxMT', 'dyMT', 'dzMT', 'sMTON', 'sMTOFF', 'MTOFFTEs','MTTR','MTFA');
                end
            elseif ~isempty(strfind(scans(s).name,'GRE_PDW')) || ~isempty(strfind(scans(s).name,'GRE_PDw'))
                img=vuOpenImage([rawFolder scans(s).name]);
                dxGRE=img.Spc(1);
                dyGRE=img.Spc(2);
                dzGRE=img.Spc(3);
                sPDW=img.Data;
                GRETR=img.Parms.repetition_time;
                
                img=loadParRec([rawFolder scans(s).name]);
                GREFAs(1)=img.imgdef.image_flip_angle_in_degrees.uniq;
                if exist(savefilename)
                    save(savefilename, 'dxGRE', 'dyGRE', 'dzGRE', 'sPDW', 'GRETR', 'GREFAs','-append');
                else
                    save(savefilename, 'dxGRE', 'dyGRE', 'dzGRE', 'sPDW', 'GRETR', 'GREFAs');
                end
            elseif ~isempty(strfind(scans(s).name,'GRE_T1W')) || ~isempty(strfind(scans(s).name,'GRE_T1w'))
                img=vuOpenImage([rawFolder scans(s).name]);
                sT1W=img.Data;
                
                img=loadParRec([rawFolder scans(s).name]);
                GREFAs(2)=img.imgdef.image_flip_angle_in_degrees.uniq;
                if exist(savefilename)
                    save(savefilename, 'GREFAs', 'sT1W','-append');
                else
                    save(savefilename, 'GREFAs', 'sT1W');
                end
            elseif strfind(scans(s).name,'AFI')
                img=vuOpenImage([rawFolder scans(s).name]);
                dxAFI=img.Spc(1);
                dyAFI=img.Spc(2);
                dzAFI=img.Spc(3);
                sAFI=img.Data;
                AFITRs=img.Parms.repetition_time;
                
                img=loadParRec([rawFolder scans(s).name]);
                AFIFA=img.imgdef.image_flip_angle_in_degrees.uniq;
                if exist(savefilename)
                    save(savefilename, 'dxAFI', 'dyAFI', 'dzAFI', 'sAFI', 'AFITRs', 'AFIFA','-append');
                else
                    save(savefilename, 'dxAFI', 'dyAFI', 'dzAFI', 'sAFI', 'AFITRs', 'AFIFA');
                end
            elseif ~isempty(strfind(scans(s).name,'Dixon')) || ~isempty(strfind(scans(s).name,'DIXON'))
                img=vuOpenImage([rawFolder scans(s).name]);
                dxmDixon=img.Spc(1);
                dymDixon=img.Spc(2);
                dzmDixon=img.Spc(3);
                smDixon=img.Data(:,:,:,:,3); %real
                smDixonp=img.Data(:,:,:,:,4); %imaginary
%                 smDixon=img.Data(:,:,:,:,2); %magnitude
%                 smDixonp=img.Data(:,:,:,:,5); %phase
                
                img=loadParRec([rawFolder scans(s).name]);
                mDixonTEs=img.imgdef.echo_time.uniq';
                if mDixonTEs(1)==0
                    mDixonTEs=mDixonTEs(2:end);
                end
                if exist(savefilename)
                    save(savefilename, 'dxmDixon', 'dymDixon', 'dzmDixon', 'smDixon', 'smDixonp', 'mDixonTEs','-append');
                else
                    save(savefilename, 'dxmDixon', 'dymDixon', 'dzmDixon', 'smDixon', 'smDixonp', 'mDixonTEs');
                end
            elseif ~isempty(strfind(scans(s).name,'T1MT')) || ~isempty(strfind(scans(s).name,'T1wMT'))
                img=vuOpenImage([rawFolder scans(s).name]);
%                 dxT1MT=img.Spc(1);
%                 dyT1MT=img.Spc(2);
%                 dzT1MT=img.Spc(3);
                MTt1wTR=img.Parms.repetition_time;
                if size(img.Data,5)>1
                    sMTt1w=img.Data(:,:,:,1,1);
                else
                    sMTt1w=img.Data(:,:,:,1);
                end
                
                img=loadParRec([rawFolder scans(s).name]);
                MTt1wTEs=img.imgdef.echo_time.uniq';
                MTt1wFA=img.imgdef.image_flip_angle_in_degrees.uniq;
                if exist(savefilename)
                    save(savefilename, 'MTt1wTR', 'sMTt1w', 'MTt1wTEs', 'MTt1wFA','-append');
%                     save(savefilename, 'dxT1MT', 'dyT1MT', 'dzT1MT', 'T1MTt1wTR', 'sMTt1w', 'MTt1wTEs', 'MTt1wFA','-append');
                else
                    save(savefilename, 'MTt1wTR', 'sMTt1w', 'MTt1wTEs', 'MTt1wFA');
%                     save(savefilename, 'dxT1MT', 'dyT1MT', 'dzT1MT', 'T1MTt1wTR', 'sMTt1w', 'MTt1wTEs', 'MTt1wFA');
                end
            elseif ~isempty(strfind(scans(s).name,'PDMT')) || ~isempty(strfind(scans(s).name,'PDwMT'))
                img=vuOpenImage([rawFolder scans(s).name]);
                MTpdwTR=img.Parms.repetition_time;
                if size(img.Data,5)>1
                    sMTpdw=img.Data(:,:,:,1,1);
                else
                    sMTpdw=img.Data(:,:,:,1);
                end
                
                img=loadParRec([rawFolder scans(s).name]);
                MTpdwTEs=img.imgdef.echo_time.uniq';
                MTpdwFA=img.imgdef.image_flip_angle_in_degrees.uniq;
                if exist(savefilename)
                    save(savefilename, 'MTpdwTR', 'sMTpdw', 'MTpdwTEs', 'MTpdwFA','-append');
                else
                    save(savefilename, 'MTpdwTR', 'sMTpdw', 'MTpdwTEs', 'MTpdwFA');
                end
            elseif strfind(scans(s).name,'DAM')
                img=vuOpenImage([rawFolder scans(s).name]);
                if ~exist('dxDAM','var')
                    dxDAM=img.Spc(1);
                    dyDAM=img.Spc(2);
                    dzDAM=img.Spc(3);
                    sDAM=img.Data;
                    
                    img=loadParRec([rawFolder scans(s).name]);
                    DAMFAs(1)=img.imgdef.image_flip_angle_in_degrees.uniq;
                else
                    sDAM=cat(4,sDAM,img.Data);
                    
                    img=loadParRec([rawFolder scans(s).name]);
                    DAMFAs(2)=img.imgdef.image_flip_angle_in_degrees.uniq;
                    if exist(savefilename)
                        save(savefilename, 'dxDAM', 'dyDAM', 'dzDAM', 'sDAM', 'DAMFAs','-append');
                    else
                        save(savefilename, 'dxDAM', 'dyDAM', 'dzDAM', 'sDAM', 'DAMFAs');
                    end
                end
            end
        end
    end
    clear dxUHR dyUHR dzUHR sUHR dxMT dyMT dzMT sMTON sMTOFF MTOFFTEs dxGRE dyGRE dzGRE sPDW GRETR GREFAs sT1w dxAFI dyAFI dzAFI sAFI AFITRs AFIFA dxmDixon dymDixon dzmDixon smDixon smDixonp mDixonTEs dxT1MT dyT1MT dzT1MT T1MTt1wTR sMTt1w MTt1wTEs MTt1wFA dxDAM dyDAM dzDAM sDAM DAMFAs
end

