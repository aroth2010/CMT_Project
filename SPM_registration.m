function []=SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)
% prefix is the prefix you want to give to the registered file 
% ref_nii_path is the path for the reference data
% mov_nii_path is the path for the moving data (registered file will be 
% saved in the same folder as the moving data)
% other_nii_path is the path for any other data that is in the same volume the moving data 

matlabbatch{1}.spm.spatial.coreg.estwrite.ref = ref_nii_path;
matlabbatch{1}.spm.spatial.coreg.estwrite.source = mov_nii_path;
matlabbatch{1}.spm.spatial.coreg.estwrite.other = other_nii_path;
matlabbatch{1}.spm.spatial.coreg.estwrite.eoptions.cost_fun = 'nmi';
matlabbatch{1}.spm.spatial.coreg.estwrite.eoptions.sep = [4 2];
matlabbatch{1}.spm.spatial.coreg.estwrite.eoptions.tol = [0.02 0.02 0.02 0.001 0.001 0.001 0.01 0.01 0.01 0.001 0.001 0.001];
matlabbatch{1}.spm.spatial.coreg.estwrite.eoptions.fwhm = [7 7];
matlabbatch{1}.spm.spatial.coreg.estwrite.roptions.interp = 4;
matlabbatch{1}.spm.spatial.coreg.estwrite.roptions.wrap = [0 0 0];
matlabbatch{1}.spm.spatial.coreg.estwrite.roptions.mask = 1;
matlabbatch{1}.spm.spatial.coreg.estwrite.roptions.prefix = prefix;

spm('defaults', 'FMRI');
spm_jobman('run', matlabbatch);
clear matlabbatch doscmd
