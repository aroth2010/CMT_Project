% %CMT_dev_images
% close all; clear; clc
% slice=20;
% pat='03';

function CMT_dev_images(pat,slice)
%%
folder = ['/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/CMT1A-R21/Scan Development/dev_' pat '/Processed/'];
body={'thigh','calf'};
imgtype={'UHR','mDixonFatFraction','MTRapp','MTRcorr','R2s','T2s','T1','B1plus','MTsat'};
range={[],[0,100],[0,100],[0,100],[0,100],[0,100],[0,3000],[0,1300],[0,100]};

addpath('/Users/alisonroth/Documents/Code/MATLAB/colormaps/');
load jetb

%% Loop over the image types for each body type
for b=1:2
    for i=1:size(imgtype,2)
        if exist([folder body{b} 'final_' imgtype{i} '.nii'])
            figure();
            img=load_nii([folder body{b} 'final_' imgtype{i} '.nii']);
            if strcmp(imgtype{i},'UHR')
                imagesc(img.img(:,:,slice)),xticklabels(''); yticklabels('')
                colormap gray
            else
                imagesc(img.img(:,:,slice),range{i})
                xticklabels('')
                yticklabels('')
                colormap(jetb)
                colorbar
            end
            mkdir([folder 'Images']);
            saveas(gcf,[folder 'Images/' body{b} '_' imgtype{i}], 'epsc');
        end
    end
end