% Yongsheng Chen <ys.chen@wayne.edu>; YC
% tested on MATLAB R2016a and R2019b w/
% Image Processing Toolbox
% Statistics and Machine Learning Toolbox

% Log:
% 8-13-2021, YC: Create.f
%MTR B1 correction and MTsat added by AR 8-30-2021

%% 
clear;close all;clc;
pat='05';
% 
% % dirOutput='C:\CMT_Project_Data\CMT0011_JH_07132021_Output\'; %Yongsheng
dirOutput=['/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/CMT1A-R21/Scan Development/phantom_' pat '/Processed/']; %Alison %dev_
% % datafile=[dirOutput,'thigh_CMT0011_JH_07132021.mat']; %Yongsheng
strSite='thigh';
datafile=[dirOutput,'CMT_dev_' pat '_' strSite '_data.mat']; %Alison %CMT_dev
savefilename=[dirOutput 'CMT_dev_' pat '_' strSite '_maps.mat'];
Registration=1; % 0 for no-coregistration
wfalgo=1; % water fat decomposition algorithm: 1=hIDEAL; 2=QPBO

%function CMT_Data_Processing(dirOutput,datafile,Registration,strSite)
%% load data
% addpath FattyRiot/;
cd('/Users/alisonroth/Documents/Code/MATLAB/CMT_Project/');
% f/w decomposition from ISMRM f/w toolbox 2012
addpath tsao_jiang/;

% download the SPM12 and install, or add it to your search path
% or turn off image alignment by Registration=0;
% addpath('..\spm12'); %Yongsheng
addpath('/Users/alisonroth/Documents/Code/MATLAB/spm12');

% load the datafile includes:
% UHR:      the ultra-high resolution gradient echo image
%           dxUHR, dyUHR, dzUHR are the voxel size; sUHR(row, column, slice) is
%           the magnitude image;
%
% MT:       Magnetization transfer on and off data
%           dxMT, dyMT, dzMT, MTOFFTEs, sMTON(row,column,slice,echo), and sMTOFF();
%
% GRE:      Gradient echo data for T1 mapping
%           dxGRE, dyGRE, dzGRE, GRETR, GREFAs, reged_MTOFFW, and reged_T1MTW
%
% AFI:      Actual Flip Angle interleaved scan for B1 mapping
%           dxAFI, dyAFI, dzAFI, sAFI(row,column,slice,TR), AFIFA, and AFITRs;
%
% mDixon:   Multi-echo gradient echo data for f/w decomposition
%           dxmDixon, dymDixon, dzmDixon, mDixonTEs, smDixon(row,column,slice,echo),
%           and the phase smDixonp();
% 
% T1MT:     T1w image acquired with similar parameters to MT image for T1
%           correction
%           dxT1MT, dyT1MT, dzT1MT, T1MTTEs, T1MTFA, T1MTTR,
%           reged_T1MTMT(row,column,slice,echo,flip angle) *The FA dimension is only
%           needed while we optimize flip angle
%
% DAM:      Double Angle Method scan for B1 mapping
%           dxDAM, dyDAM, dzDAM, DAMFAs, sDAM(row,column,slice,flip angle)

load(datafile);

%% AFI B1 mapping
minB1=0.3;
maxB1=2.0;
r=sAFI(:,:,:,2)./sAFI(:,:,:,1);
n=AFITRs(2)/AFITRs(1);
aFA=abs(acos((n.*r-1)./(n-r)));
AFIB1map=aFA./(AFIFA.*pi./180);
AFIB1map(isnan(AFIB1map))=0;
AFIB1map(AFIB1map<minB1)=0;
AFIB1map(AFIB1map>maxB1)=0;

if size(AFIB1map,3)>size(sMTON,3)
    diff=size(AFIB1map,3)-size(sMTON,3);
    start=diff/2;
    stop=size(AFIB1map,3)-diff/2-1;
    AFIB1map=AFIB1map(:,:,start:stop);
end

%% DAM B1 mapping
minB1=0.3;
maxB1=2.0;
r=sDAM(:,:,:,1)./(2*sDAM(:,:,:,1));
DAMB1map=abs(acos(r)/(min(DAMFAs)*pi/180));
DAMB1map(isnan(DAMB1map))=0;
DAMB1map(DAMB1map<minB1)=0;
DAMB1map(DAMB1map>maxB1)=0;

if size(DAMB1map,3)>size(sMTON,3)
    diff=size(DAMB1map,3)-size(sMTON,3);
    start=diff/2;
    stop=size(DAMB1map,3)-diff/2-1;
    DAMB1map=DAMB1map(:,:,start:stop);
end

%% mDixon f/w decomp. using Hierarchical IDEAL
dimmDixon=size(smDixon);

compim=smDixon+1i*smDixonp; %real + imaginary
%compim=smDixon.*exp(1i*smDixonp);
images=single(zeros(dimmDixon(1),dimmDixon(2),dimmDixon(3),1,dimmDixon(4)));
for e=1:dimmDixon(4)
    images(:,:,:,1,e)=single(compim(:,:,:,e));
end
imDataParams.images=images;
imDataParams.TE=mDixonTEs/1000.0;%ms to s
imDataParams.FieldStrength=3.0;%3T
imDataParams.PrecessionIsClockwise=1;

clear algoParams;
algoParams.species(1).name = 'Water';
algoParams.species(1).frequency = 4.7;
algoParams.species(1).relAmps = 1;
algoParams.species(2).name = 'Fat (6 peaks)';
algoParams.species(2).frequency = [0.9000    1.3000    2.1000    2.7600    4.3100    5.3000];  % ppm
algoParams.species(2).relAmps =   [0.0871    0.6937    0.1281    0.0040    0.0390    0.0480];

if wfalgo==1 %hIDEAL
    % hIDEAL f/w decomposition from ISMRM f/w toolbox 2012
    
    algoParams.MinFractSizeToDivide = 0.01;
    algoParams.MaxNumDiv = 7;
    outParams = fw_i2cm0c_3pluspoint_tsaojiang(imDataParams, algoParams);
    
else %berglund QPBO
    % this is downloaded from:
    % https://github.com/welcheb/FattyRiot
    % the ISRMM 2012 toolbox version has a bug in DixonApp.m, which is
    % corrected in below:
    % https://github.com/welcheb/fw_i3cm1i_3pluspoint_berglund_QPBO
    
    algoParams.decoupled_estimation = true; % flag for decoupled R2 estimation
    algoParams.Fibonacci_search = true; % flag for Fibonacci search
    algoParams.B0_smooth_in_stack_direction = false; % flag for B0 smooth in stack direction
    algoParams.multigrid = true; % flag for multi-level resolution pyramid
    algoParams.estimate_R2 = true; % flag to estimate R2star
    algoParams.verbose = true; % flag for verbose status messages (default false)
    algoParams.process_in_3D = false; % flag to process in 3D (default true)
    algoParams.R2star_calibration = true; % flag to perform R2* calibration (default false)
    algoParams.ICM_iterations = 2; % ICM iterations
    algoParams.num_B0_labels = 100; % number of discretized B0 values
    algoParams.mu = 10; % regularization parameter
    algoParams.R2_stepsize = 1; % R2 stepsize in s^-1
    algoParams.max_R2 = 120; % maximum R2 in s^-1
    algoParams.max_label_change = 0.1; % 
    algoParams.fine_R2_stepsize = 1.0; % Fine stepsize for discretization of R2(*) [s^-1] (used in decoupled fine-tuning step)
    algoParams.coarse_R2_stepsize = 10.0; % Coarse stepsize for discretization of R2(*) [s^-1] (used for joint estimation step, value 0.0 --> Decoupled estimation only
    algoParams.water_R2 = 0.0; % Water R2 [sec-1]
    algoParams.fat_R2s = zeros(1,9); % fat peak R2s [s^-1]
    algoParams.R2star_calibration_max = 800; % max R2* to use for calibration [s^-1] (default 800)
    algoParams.R2star_calibration_cdf_threshold = 0.98; %% threshold for R2* calibration cumulative density function [0,1] (default 0.98)

%     outParams = fw_i3cm1i_3pluspoint_berglund_QPBO(imDataParams,algoParams);
    outParams = FattyRiot_fw_i3cm1i_3pluspoint_berglund_QPBO(imDataParams,algoParams);
end

mDixonW = abs(outParams.species(1).amps);
mDixonF = abs(outParams.species(2).amps);

mDixonFW=zeros(dimmDixon(1),dimmDixon(2),dimmDixon(3)*2);
mDixonFW(:,:,[1:dimmDixon(3)])=mDixonF;
mDixonFW(:,:,[1:dimmDixon(3)]+dimmDixon(3))=mDixonW;

mDixonFW = mDixonFW - min(mDixonFW(:));
mDixonFW = mDixonFW / max(mDixonFW(:));
mDixonF = mDixonFW(:,:,[1:dimmDixon(3)]);
mDixonW = mDixonFW(:,:,[1:dimmDixon(3)]+dimmDixon(3));
mDixonFF = mDixonF./(mDixonW+mDixonF)*100;
mDixonFF(isnan(mDixonFF))=0;

% imagesc(mDixonFF(:,:,10)); colorbar

%% R2star, T2star from MTOFF
minT2s=5; %5 ms as the minimum t2*
maxT2s=200; %200 ms as the maximum t2*
maxR2s=1/minT2s;
minR2s=1/maxT2s;

[R2sMTOFF, ~] = STAGE_R2star(sMTOFF, MTOFFTEs, length(MTOFFTEs), ones(size(sMTOFF(:,:,:,1))));

T2sMTOFF=1./R2sMTOFF;

T2sMTOFF(isnan(T2sMTOFF))=0;
T2sMTOFF(T2sMTOFF<minT2s)=0;
T2sMTOFF(T2sMTOFF>maxT2s)=0;

R2sMTOFF(isnan(R2sMTOFF))=0;
R2sMTOFF(R2sMTOFF<minR2s)=0;
R2sMTOFF(R2sMTOFF>maxR2s)=0;

%% image resigtration

if Registration >0 %SPM12 registration
    if ~exist("sUHR","var")
        sUHR=sMTOFF(:,:,:,1);
        dxUHR=dxMT;
        dyUHR=dyMT;
        dzUHR=dzMT;
        eUHR=1;
        disp('Delete all UHR files at end of analysis.')
    else eUHR=0;
    end

    sUHRgre=imresize(sUHR,dxUHR/dxGRE);
    sUHRmt=imresize(sUHR,dxUHR/dxMT);
    sUHRmDixon=imresize(sUHR,dxUHR/dxmDixon);
    sUHRafi=imresize(sUHR,dxUHR/dxAFI);

    mat2analyze_file(sUHR,[dirOutput,strSite,'UHR.nii'],[dxUHR dyUHR dzUHR],[0,0,0],16);
    mat2analyze_file(sUHRgre,[dirOutput,strSite,'UHRgresize.nii'],[dxGRE dyGRE dzGRE],[0,0,0],16);
    mat2analyze_file(sUHRmt,[dirOutput,strSite,'UHRmtsize.nii'],[dxMT dyMT dzMT],[0,0,0],16);
    mat2analyze_file(sUHRmt,[dirOutput,strSite,'UHRt1mtsize.nii'],[dxMT dxMT dxMT],[0,0,0],16);
    mat2analyze_file(sUHRmDixon,[dirOutput,strSite,'UHRmDixonsize.nii'],[dxmDixon dymDixon dzmDixon],[0,0,0],16);
    mat2analyze_file(sUHRafi,[dirOutput,strSite,'UHRafisize.nii'],[dxAFI dyAFI dzAFI],[0,0,0],16);
    mat2analyze_file(sUHRafi,[dirOutput,strSite,'UHRdamsize.nii'],[dxDAM dyDAM dzDAM],[0,0,0],16);

    prefix='regToUHR_';

    mat2analyze_file(smDixon(:,:,:,2),[dirOutput,strSite,'mDixonIP.nii'],[dxmDixon dymDixon dzmDixon],[0,0,0],16);
    mat2analyze_file(smDixon(:,:,:,4),[dirOutput,strSite,'mDixonOP.nii'],[dxmDixon dymDixon dzmDixon],[0,0,0],16);
    mat2analyze_file(mDixonW*1000,[dirOutput,strSite,'mDixonW.nii'],[dxmDixon dymDixon dzmDixon],[0,0,0],16);
    mat2analyze_file(mDixonF*1000,[dirOutput,strSite,'mDixonF.nii'],[dxmDixon dymDixon dzmDixon],[0,0,0],16);
    mat2analyze_file(mDixonFF,[dirOutput,strSite,'mDixonFF.nii'],[dxmDixon dymDixon dzmDixon],[0,0,0],16);
    
    mat2analyze_file(sPDW,[dirOutput,strSite,'PDW.nii'],[dxGRE dyGRE dzGRE],[0,0,0],16);
    mat2analyze_file(sT1W,[dirOutput,strSite,'T1W.nii'],[dxGRE dyGRE dzGRE],[0,0,0],16);
    
    mat2analyze_file(R2sMTOFF*1000,[dirOutput,strSite,'R2sMTOFF.nii'],[dxMT dyMT dzMT],[0,0,0],16);
    mat2analyze_file(T2sMTOFF,[dirOutput,strSite,'T2sMTOFF.nii'],[dxMT dyMT dzMT],[0,0,0],16);
    mat2analyze_file(sMTON(:,:,:,1),[dirOutput,strSite,'MTON.nii'],[dxMT dyMT dzMT],[0,0,0],16);
    mat2analyze_file(sMTOFF(:,:,:,1),[dirOutput,strSite,'MTOFF.nii'],[dxMT dyMT dzMT],[0,0,0],16);
    mat2analyze_file(sMTt1w,[dirOutput,strSite,'T1MT.nii'],[dxMT dxMT dxMT],[0,0,0],16);

    mat2analyze_file(sAFI(:,:,:,1),[dirOutput,strSite,'AFITR1.nii'],[dxMT dxMT dxMT],[0,0,0],16);%[dxAFI dyAFI dzAFI],16);
    mat2analyze_file(AFIB1map*1000,[dirOutput,strSite,'AFIB1map.nii'],[dxMT dxMT dxMT],[0,0,0],16);%[dxAFI dyAFI dzAFI],16);
    mat2analyze_file(sDAM(:,:,:,1),[dirOutput,strSite,'DAMFA1.nii'],[dxMT dxMT dxMT],[0,0,0],16);%[dxAFI dyAFI dzAFI],16);
    mat2analyze_file(DAMB1map*1000,[dirOutput,strSite,'DAMB1map.nii'],[dxMT dxMT dxMT],[0,0,0],16);%[dxAFI dyAFI dzAFI],16);
    
    %reg PDW to UHR
    ref_nii_path={[dirOutput,strSite,'UHRgresize.nii',',1']};
    mov_nii_path={[dirOutput,strSite,'PDW.nii',',1']};
    other_nii_path={[]};
    SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)

    %reg T1W to UHR
    ref_nii_path={[dirOutput,strSite,'UHRgresize.nii',',1']};
    mov_nii_path={[dirOutput,strSite,'T1W.nii',',1']};
    other_nii_path={[]};
    SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)

    %reg mDixon to UHR
    ref_nii_path={[dirOutput,strSite,'UHRmDixonsize.nii',',1']};
    mov_nii_path={[dirOutput,strSite,'mDixonIP.nii',',1']};
    other_nii_path={
        [dirOutput,strSite,'mDixonOP.nii',',1']
        [dirOutput,strSite,'mDixonW.nii',',1']
        [dirOutput,strSite,'mDixonF.nii',',1']
        [dirOutput,strSite,'mDixonFF.nii',',1']
        };
    SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)
    
    %reg MTON to UHR
    ref_nii_path={[dirOutput,strSite,'UHRmtsize.nii',',1']};
    mov_nii_path={[dirOutput,strSite,'MTON.nii',',1']};
    other_nii_path={[]};
    SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)

    %reg MTOFF to UHR
    ref_nii_path={[dirOutput,strSite,'UHRmtsize.nii',',1']};
    mov_nii_path={[dirOutput,strSite,'MTOFF.nii',',1']};
    other_nii_path={
        [dirOutput,strSite,'R2sMTOFF.nii',',1']
        [dirOutput,strSite,'T2sMTOFF.nii',',1']
        };
    SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)

    %reg T1MT to UHR
    ref_nii_path={[dirOutput,strSite,'UHRt1mtsize.nii',',1']};
    mov_nii_path={[dirOutput,strSite,'T1MT.nii',',1']};
    other_nii_path={[]};
    SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)
    
    %reg AFI to UHR
    ref_nii_path={[dirOutput,strSite,'UHRafisize.nii',',1']};
    mov_nii_path={[dirOutput,strSite,'AFITR1.nii',',1']};
    other_nii_path={
        [dirOutput,strSite,'AFIB1map.nii',',1']
        };
    SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)
    
    %reg DAM to UHR
    ref_nii_path={[dirOutput,strSite,'UHRdamsize.nii',',1']};
    mov_nii_path={[dirOutput,strSite,'DAMFA1.nii',',1']};
    other_nii_path={
        [dirOutput,strSite,'DAMB1map.nii',',1']
        };
    SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)


    %load reged data for further processing  
    reged_PDW=read_analyze([dirOutput,prefix,strSite,'PDW.nii']);
    reged_T1W=read_analyze([dirOutput,prefix,strSite,'T1W.nii']);
    
    reged_mDixonIP=read_analyze([dirOutput,prefix,strSite,'mDixonIP.nii']);
    reged_mDixonOP=read_analyze([dirOutput,prefix,strSite,'mDixonOP.nii']);
    reged_mDixonW=read_analyze([dirOutput,prefix,strSite,'mDixonW.nii']);
    reged_mDixonF=read_analyze([dirOutput,prefix,strSite,'mDixonF.nii']);
    reged_mDixonFF=read_analyze([dirOutput,prefix,strSite,'mDixonFF.nii']);
    
    reged_R2sMTOFF=read_analyze([dirOutput,prefix,strSite,'R2sMTOFF.nii']);
    reged_T2sMTOFF=read_analyze([dirOutput,prefix,strSite,'T2sMTOFF.nii']);
    reged_MTON=read_analyze([dirOutput,prefix,strSite,'MTON.nii']);
    reged_MTOFF=read_analyze([dirOutput,prefix,strSite,'MTOFF.nii']);
    reged_T1MT=read_analyze([dirOutput,prefix,strSite,'T1MT.nii']);
    
    reged_AFIB1map=read_analyze([dirOutput,prefix,strSite,'AFIB1map.nii'])/1000;
    reged_DAMB1map=read_analyze([dirOutput,prefix,strSite,'DAMB1map.nii']);
    
    delete ([dirOutput,strSite,'UHR.nii']);
    delete([dirOutput,strSite,'UHRgresize.nii']);
    delete([dirOutput,strSite,'UHRmtsize.nii']);
    delete([dirOutput,strSite,'UHRmDixonsize.nii']);
    delete([dirOutput,strSite,'UHRafisize.nii']);
    delete([dirOutput,strSite,'UHRdamsize.nii']);
    delete([dirOutput,strSite,'UHRt1mtsize.nii']);
    
    delete([dirOutput,strSite,'mDixonIP.nii']);
    delete([dirOutput,strSite,'mDixonOP.nii']);
    delete([dirOutput,strSite,'mDixonW.nii']);
    delete([dirOutput,strSite,'mDixonF.nii']);
    delete([dirOutput,strSite,'mDixonFF.nii']);
    
    delete([dirOutput,strSite,'PDW.nii']);
    delete([dirOutput,strSite,'T1W.nii']);
    
    delete([dirOutput,strSite,'R2sMTOFF.nii']);
    delete([dirOutput,strSite,'T2sMTOFF.nii']);
    delete([dirOutput,strSite,'MTON.nii']);
    delete([dirOutput,strSite,'MTOFF.nii']);
    
    delete([dirOutput,strSite,'AFITR1.nii']);
    delete([dirOutput,strSite,'AFIB1map.nii']);
    delete([dirOutput,strSite,'DAMFA1.nii']);
    delete([dirOutput,strSite,'DAMB1map.nii']);
    delete([dirOutput,strSite,'T1MT.nii']);
    
    delete([dirOutput,prefix,strSite,'*.nii']);

else
    
    %no coregistration, original data
    try reged_PDW=reged_MTOFFW; end
    try reged_T1W=reged_T1MTW; end
    
    reged_mDixonIP=smDixon(:,:,:,2);
    reged_mDixonOP=smDixon(:,:,:,4);
    reged_mDixonW=mDixonW*1000;
    reged_mDixonF=mDixonF*1000;
    reged_mDixonFF=mDixonFF;
    
    reged_R2sMTOFF=R2sMTOFF*1000;
    reged_T2sMTOFF=T2sMTOFF;
    reged_MTON=sMTON(:,:,:,1);
    reged_MTOFF=sMTOFF(:,:,:,1);
    
    reged_AFIB1map=AFIB1map;
    reged_DAMB1map=DAMB1map;
    if size(sMTt1w,1)~=size(reged_MTOFF,1)
        reged_T1MT=imresize(sMTt1w,size(reged_MTOFF,1)/size(sMTt1w,1));
    else
        reged_T1MT=sMTt1w;
    end
end

try reged_PDW(isnan(reged_PDW))=0; end
try reged_T1W(isnan(reged_T1W))=0; end

reged_mDixonIP(isnan(reged_mDixonIP))=0;
reged_mDixonOP(isnan(reged_mDixonOP))=0;
reged_mDixonW(isnan(reged_mDixonW))=0;
reged_mDixonF(isnan(reged_mDixonF))=0;
reged_mDixonFF(isnan(reged_mDixonFF))=0;

reged_R2sMTOFF(isnan(reged_R2sMTOFF))=0;
reged_T2sMTOFF(isnan(reged_T2sMTOFF))=0;
reged_MTON(isnan(reged_MTON))=0;
reged_MTOFF(isnan(reged_MTOFF))=0;
reged_T1MT(isnan(reged_T1MT))=0;

reged_AFIB1map(isnan(reged_AFIB1map))=0;
reged_DAMB1map(isnan(reged_DAMB1map))=0;
disp('Registration finished');

%% Mask
sNoise=35/100; %magnitude noise threshold
dimMT=size(reged_MTOFF);
Mask_fatsat=ones(dimMT(1),dimMT(2),dimMT(3));
Mask_fatsat(reged_MTOFF/max(reged_MTOFF(:))<sNoise)=0;% mask removing noise region

dimGRE=size(reged_PDW);
Mask_nofatsat=ones(dimGRE(1),dimGRE(2),dimGRE(3));
Mask_nofatsat(reged_PDW<sNoise)=0;% mask removing noise region

%% T1 mapping
magGRE=zeros(dimGRE(1),dimGRE(2),dimGRE(3),2);
magGRE(:,:,:,1)=reged_PDW;
magGRE(:,:,:,2)=reged_T1W;
scale=dimGRE(1)/size(reged_AFIB1map,1);
afi_B1map=imresize(reged_AFIB1map,scale);
afi_B1map(isnan(afi_B1map))=0;

scale=dimGRE(1)/size(reged_DAMB1map,1);
dam_B1map=imresize(reged_DAMB1map,scale);
dam_B1map(isnan(dam_B1map))=0;
k=ones(dimGRE(1),dimGRE(2),dimGRE(3),2);
k(:,:,:,1)=afi_B1map;
k(:,:,:,2)=afi_B1map;

[T1,p0,rsq]=fn_vfa_t1mapping(magGRE,GREFAs,k,GRETR,20000,20000,1);

T1(Mask_nofatsat<1)=0;
p0(Mask_nofatsat<1)=0;
T1(isnan(T1))=0;
p0(isnan(p0))=0;
mat2analyze_file(T1,[dirOutput,strSite,'final_T1.nii'],[dxGRE dyGRE dzGRE],[0,0,0],16);
mat2analyze_file(p0,[dirOutput,strSite,'final_p0.nii'],[dxGRE dyGRE dzGRE],[0,0,0],16);
mat2analyze_file(afi_B1map.*Mask_nofatsat.*1000,[dirOutput,strSite,'final_afiB1plus.nii'],[dxGRE dyGRE dzGRE],[0,0,0],4);
mat2analyze_file(dam_B1map.*Mask_nofatsat.*1000,[dirOutput,strSite,'final_damB1plus.nii'],[dxGRE dyGRE dzGRE],[0,0,0],4);
mat2analyze_file(Mask_nofatsat.*1000,[dirOutput,strSite,'final_Mask_nofatsat.nii'],[dxGRE dyGRE dzGRE],[0,0,0],4);
mat2analyze_file(Mask_fatsat.*1000,[dirOutput,strSite,'final_Mask_fatsat.nii'],[dxMT dyMT dzMT],[0,0,0],4);

scale=dimMT(1)/size(reged_AFIB1map,1);
afi_B1map=imresize(reged_AFIB1map,scale);
afi_B1map(isnan(afi_B1map))=0;

scale=dimMT(1)/size(reged_DAMB1map,1);
dam_B1map=imresize(reged_DAMB1map,scale);
dam_B1map(isnan(dam_B1map))=0;

%% MTR
mtr=reged_MTON./reged_MTOFF;
mtr(isnan(mtr))=0;
mtr(mtr>2)=0;
mtr(mtr<0.1)=0;
mtr(isnan(mtr))=0;
mtr=(1-mtr)*100;

% MTR correction using AFI - could use DAM instead
B1Err=(afi_B1map-AFIFA*pi/180)/(AFIFA*pi/180); %Check that afi_B1map isn't >1000
p=polyfit(mtr(Mask_fatsat>0),B1Err(Mask_fatsat>0),1);
k=p(1)/p(2);
mtrc=mtr./(1+k*B1Err);

%MTsat calculation
if MTFA>4
    MTFA=MTFA*pi/180;
end
if MTt1wFA>4
    MTt1wFA=MTt1wFA*pi/180;
end

A=(MTTR*MTt1wFA/MTFA - MTt1wTR*MTFA/MTt1wFA).*(reged_MTOFF.*reged_T1MT./(MTTR*MTt1wFA.*reged_T1MT-MTt1wTR*MTFA.*reged_MTOFF));
R1=0.5*(reged_T1MT*MTt1wFA/MTt1wTR - reged_MTOFF*MTFA/MTTR)./(reged_MTOFF/MTFA-reged_T1MT/MTt1wFA);
mtsat=100*((A*MTFA./reged_MTON-1).*R1*MTTR - ((MTFA)^2)/2);

mtsat(isnan(mtsat))=0;
mtsat(mtsat<0)=0;
mtsat(mtsat>100)=100;


%Clean up
% mtr=mtr.*Mask_fatsat;
% mtrc=mtrc.*Mask_fatsat;
% mtsat=mtsat.*Mask_fatsat;

mtr(isnan(mtr))=0;
mtr(mtr<0)=0;
mtr(mtr>100)=100;

mtrc(isnan(mtrc))=0;
mtrc(mtrc<0)=0;
mtrc(mtrc>100)=100;

mat2analyze_file(mtr,[dirOutput,strSite,'final_MTRapp.nii'],[dxMT dyMT dzMT],[0,0,0],16);
mat2analyze_file(mtrc,[dirOutput,strSite,'final_MTRcorr.nii'],[dxMT dyMT dzMT],[0,0,0],16);
mat2analyze_file(mtsat,[dirOutput,strSite,'final_MTsat.nii'],[dxMT dyMT dzMT],[0,0,0],16);

save(savefilename, 'mtr', 'mtrc', 'mtsat');

%% save other files
if eUHR==1 
    mat2analyze_file(sUHR,[dirOutput,strSite,'final_UHR.nii'],[dxUHR dyUHR dzUHR],[0,0,0],16);
    save(savefilename, 'sUHR','-append');
end

mat2analyze_file(reged_PDW,[dirOutput,strSite,'final_PDW.nii'],[dxGRE dyGRE dzGRE],[0,0,0],16);
mat2analyze_file(reged_T1W,[dirOutput,strSite,'final_T1W.nii'],[dxGRE dyGRE dzGRE],[0,0,0],16);
save(savefilename, 'reged_PDW', 'reged_T1W', '-append');

mat2analyze_file(reged_MTOFF,[dirOutput,strSite,'final_MTOFF.nii'],[dxMT dyMT dzMT],[0,0,0],16);
mat2analyze_file(reged_MTON,[dirOutput,strSite,'final_MTON.nii'],[dxMT dyMT dzMT],[0,0,0],16);

mat2analyze_file(reged_R2sMTOFF.*Mask_fatsat,[dirOutput,strSite,'final_R2s.nii'],[dxMT dyMT dzMT],[0,0,0],16);
mat2analyze_file(reged_T2sMTOFF.*Mask_fatsat,[dirOutput,strSite,'final_T2s.nii'],[dxMT dyMT dzMT],[0,0,0],16);
save(savefilename, 'reged_R2sMTOFF', 'reged_T2sMTOFF', '-append');

dimmDixon=size(reged_mDixonW);
maskmDixon=ones(dimmDixon(1),dimmDixon(2),dimmDixon(3));
maskmDixon(reged_mDixonIP<sNoise)=0;
mat2analyze_file(reged_mDixonIP,[dirOutput,strSite,'final_mDixonInPhaseImage.nii'],[dxmDixon dymDixon dzmDixon],[0,0,0],16);
mat2analyze_file(reged_mDixonOP,[dirOutput,strSite,'final_mDixonOutOfPhaseImage.nii'],[dxmDixon dymDixon dzmDixon],[0,0,0],16);
mat2analyze_file(reged_mDixonW,[dirOutput,strSite,'final_mDixonWaterImage.nii'],[dxmDixon dymDixon dzmDixon],[0,0,0],16);
mat2analyze_file(reged_mDixonF,[dirOutput,strSite,'final_mDixonFatImage.nii'],[dxmDixon dymDixon dzmDixon],[0,0,0],16);
mat2analyze_file(reged_mDixonFF.*maskmDixon,[dirOutput,strSite,'final_mDixonFatFraction.nii'],[dxmDixon dymDixon dzmDixon],[0,0,0],16);
save(savefilename, 'reged_mDixonIP','reged_mDixonOP','reged_mDixonW','reged_mDixonF','reged_mDixonFF','-append');

save(savefilename, 'reged_AFIB1map','reged_DAMB1map','-append');
close

%%
%return;