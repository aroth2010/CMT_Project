% Yongsheng Chen <ys.chen@wayne.edu>
% 6/3/2022
%
% DTI automatic processing pipeline using DSI Studio
%
% Download DSI Studio here at:
% https://dsi-studio.labsolver.org/

%%
clear; close all; clc;

dirPatient='C:\CMT_Project_Data\CMT0127_LH_05092022\';
strSite='calf_';
strSeqName='ep2d_diff20_new';

% function fn_DSIstudio_Pipeline(dirPatient,strSite,strSeqName)

%%
dirOutput=[dirPatient,'Processed_Data\'];
if 0 == exist(dirOutput,'dir')
    mkdir(dirOutput) 
end

strDWI=[strSite,strSeqName];
filesAll=dir(dirPatient);
nFiles=length(filesAll);
dirDWI='';
for i=1:nFiles
    strname=filesAll(i).name;
    if strcmp(strname, '.') == 0 && strcmp(strname,'..') == 0 && filesAll(i).isdir == 1
        if strcmp(dirDWI,'') == 1 && ~isempty(strfind(strname,strDWI))
           dirDWI=strname; 
           snDWI=GetSN(dirDWI);
        end
    end
end

diDWI=dicominfo([dirPatient,dirDWI,'\',snDWI,'1.dcm']);

dxDWI=diDWI.PixelSpacing(1);
dyDWI=diDWI.PixelSpacing(2);
dzDWI=diDWI.SliceThickness;
rowDWIlarge=diDWI.Rows;
columnDWIlarge=diDWI.Columns;

rowDWI=diDWI.AcquisitionMatrix(1);
columnDWI=diDWI.AcquisitionMatrix(1);
sliceDWI=diDWI.Private_0019_100a;

nd=length(dir([dirPatient,dirDWI,'\*.dcm']));

Gx=zeros(nd,1);
Gy=zeros(nd,1);
Gz=zeros(nd,1);
Bvalue=zeros(nd,1);
Gnorm=zeros(nd,1);
Delta=zeros(nd,1);
delta=zeros(nd,1);
TE=zeros(nd,1);

sDWI=zeros(rowDWI,columnDWI,sliceDWI,nd);
nsub=rowDWIlarge/rowDWI;
gyro = 42.57; % kHz/mT

for i=1:nd
   strfile=[dirPatient,dirDWI,'\',snDWI,num2str(i),'.dcm'];
   mosaic=double(dicomread(strfile));
   di=dicominfo(strfile);
   
   TE(i)=di.EchoTime*1e-3;
   Bvalue(i)=di.Private_0019_100c*1e-6;
   Gnorm(i)=sqrt(Bvalue(i)/((2*pi*gyro*delta(i))^2*(Delta(i)-delta(i)/3)));
   if isfield(di,'Private_0019_100e')
       Gx(i)=di.Private_0019_100e(1);
       Gy(i)=di.Private_0019_100e(2);
       Gz(i)=di.Private_0019_100e(3);
   end
   
   for j=1:sliceDWI
       x=floor((double(j)-0.1)/double(nsub))*rowDWI+1;
       y=mod(j,nsub);
       if y==0
           y=nsub;
       end
       y=(y-1)*columnDWI+1;
       sDWI(:,:,j,i)=mosaic(x:x+rowDWI-1,y:y+columnDWI-1);
   end
end

%% save b0 image and the DTI mask
nex=0;
b0=zeros(rowDWI,columnDWI,sliceDWI);
for i=1:nd
    if Bvalue(i)==0 && Gx(i)==0 && Gy(i)==0 && Gz(i)==0
        b0=b0+sDWI(:,:,:,i);
        nex=nex+1;
    end
end
b0=b0/nex;
maskDWI=zeros(rowDWI,columnDWI,sliceDWI);
maskDWI(b0>=10)=1;%%%% noise level

mat2analyze_file(b0,[dirOutput,strSite,'final_DTI_B0.nii'],[dxDWI dyDWI dzDWI],16);

%this flip is for matching the DSIstudio data
mat2analyze_file(flip(maskDWI,2),[dirOutput,strSite,'dti_mask_flipped.nii'],[dxDWI dyDWI dzDWI],16);

%% save averaged folder, mosaic image
dirDWIavg=[dirPatient,dirDWI,'_Averaged'];
if 0 == exist(dirDWIavg,'dir')
    mkdir(dirDWIavg) 
end

ndiff=nd/nex;

for i=1:ndiff
    di=dicominfo([dirPatient,dirDWI,'\',snDWI,num2str(i),'.dcm']);
    data=zeros(di.Rows,di.Columns,nex);
    for j=1:nex
        data(:,:,j)=double(dicomread([dirPatient,dirDWI,'\',snDWI,num2str(i+(j-1)*ndiff),'.dcm']));
    end
    avg=mean(data,3);
    dicomwrite(int16(avg),[dirDWIavg,'\',snDWI,num2str(i),'.dcm'],'WritePrivate', true,di);
end

%% DSI Studio commandline processing
dsi_studio='C:\dsi_studio_win\dsi_studio';

%convert dicom to src
strsrc=[' --source=',dirPatient,dirDWI];
strout=[' --output=',dirOutput,strSite,'dti.src.gz'];
doscmd=[dsi_studio,' --action=src',strsrc,strout];
dos(doscmd);

%dti fitting
strsrc=[' --source=',dirOutput,strSite,'dti.src.gz'];
strmask=[' --mask=',dirOutput,strSite,'dti_mask_flipped.nii'];
strmethod=' --method=1';%dti
doscmd=[dsi_studio,' --action=rec',strsrc,strmask,strmethod];
doscmd=['dsi_studio --action=rec',strsrc,strmask,strmethod];
dos(doscmd);

%export maps
strsrc=[' --source=',dirOutput,strSite,'dti.src.gz.dti.fib.gz'];
strout=' --export=fa,ad,rd,md';
doscmd=[dsi_studio, ' --action=exp',strsrc,strout];
dos(doscmd);

%% save final dti maps
strfa=gunzip([dirOutput,strSite,'dti.src.gz.dti.fib.gz.fa.nii.gz']);
strmd=gunzip([dirOutput,strSite,'dti.src.gz.dti.fib.gz.md.nii.gz']);
strad=gunzip([dirOutput,strSite,'dti.src.gz.dti.fib.gz.ad.nii.gz']);
strrd=gunzip([dirOutput,strSite,'dti.src.gz.dti.fib.gz.rd.nii.gz']);

%flip back for the final results
fa=flip(read_analyze(char(strfa)),2);
md=flip(read_analyze(char(strmd)),2);
ad=flip(read_analyze(char(strad)),2);
rd=flip(read_analyze(char(strrd)),2);

mat2analyze_file(fa.*maskDWI*1000,[dirOutput,strSite,'final_DTI_FA.nii'],[dxDWI dyDWI dzDWI],16);
mat2analyze_file(md.*maskDWI*1000,[dirOutput,strSite,'final_DTI_MD.nii'],[dxDWI dyDWI dzDWI],16);
mat2analyze_file(ad.*maskDWI*1000,[dirOutput,strSite,'final_DTI_AD.nii'],[dxDWI dyDWI dzDWI],16);
mat2analyze_file(rd.*maskDWI*1000,[dirOutput,strSite,'final_DTI_RD.nii'],[dxDWI dyDWI dzDWI],16);

%delete temporary files
delete([dirOutput,strSite,'dti*.*']);

