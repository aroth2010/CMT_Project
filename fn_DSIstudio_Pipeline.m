% Yongsheng Chen <ys.chen@wayne.edu>
% 6/3/2022
%
% DTI automatic processing pipeline using DSI Studio
%
% Updated 7/19/2022 by Alison Roth <alison.roth@barrowneuro.org> for use
% with ParRec Files
%
% Download DSI Studio here at:
% https://dsi-studio.labsolver.org/

%%
clear; close all; clc;

pat='dev_06';
dirPatient=['/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/CMT1A-R21/Scan Development/' pat '/'];
dirRaw=[dirPatient 'Raw/'];
strSite='thigh';
strSeqName=[pat '_DTI1_20_' strSite '.PAR'];
eddyname=[pat '_DTI1_EDDY_' strSite '.PAR'];

% function fn_DSIstudio_Pipeline(dirPatient,strSite,strSeqName)

%%
dirOutput=[dirPatient,'Processed/'];
if 0 == exist(dirOutput,'dir')
    mkdir(dirOutput) 
end

addpath('/Users/alisonroth/Documents/Code/MATLAB/vuTools/');
addpath('/Users/alisonroth/Documents/Code/MATLAB/Data_Load_Save');

% strDWI=strSeqName;
% filesAll=dir(dirPatient);
% nFiles=length(filesAll);
% dirDWI='';
% for i=1:nFiles
%     strname=filesAll(i).name;
%     if strcmp(strname, '.') == 0 && strcmp(strname,'..') == 0 && filesAll(i).isdir == 1
%         if strcmp(dirDWI,'') == 1 && ~isempty(strfind(strname,strDWI))
%            dirDWI=strname; 
%            snDWI=GetSN(dirDWI);
%         end
%     end
% end
% 
% diDWI=dicominfo([dirPatient,dirDWI,'\',snDWI,'1.dcm']);

% dxDWI=diDWI.PixelSpacing(1);
% dyDWI=diDWI.PixelSpacing(2);
% dzDWI=diDWI.SliceThickness;
% rowDWIlarge=diDWI.Rows;
% columnDWIlarge=diDWI.Columns;

img=vuOpenImage([dirRaw strSeqName]);
dxDWI=img.Spc(1);
dyDWI=img.Spc(2);
dzDWI=img.Spc(3);
rowDWIlarge=size(img.Data,1);
columnDWIlarge=size(img.Data,2);

% rowDWI=diDWI.AcquisitionMatrix(1);
% columnDWI=diDWI.AcquisitionMatrix(1);
% sliceDWI=diDWI.Private_0019_100a;

rowDWI=img.Parms.scan_resolution(1);
columnDWI=img.Parms.scan_resolution(2);

sDWI=cat(4,img.Data(:,:,:,1:end-2),img.Data(:,:,:,end));

img2=loadParRec([dirRaw strSeqName]);
sliceDWI=img2.imgdef.slice_thickness_in_mm.uniq;

nd=size(sDWI,4);

% Gx=img2.imgdef.diffusion_ap_fh_rl.uniq(:,1);
% Gy=img2.imgdef.diffusion_ap_fh_rl.uniq(:,2);
% Gz=img2.imgdef.diffusion_ap_fh_rl.uniq(:,3);
% Bvalue=ones(size(Gz))*max(img2.imgdef.diffusion_b_factor.uniq);
% % Gnorm=zeros(nd,1);
% % Delta=zeros(nd,1);
% % delta=zeros(nd,1);
% % TE=zeros(nd,1);
% 
% btable=[Bvalue,Gx,Gy,Gz];
% 
% b=find(btable(:,2)==0);
% btable(b,:)=0;
b0=img.Data(:,:,:,end);

% if size(sDWI,4)==21
%     btable=[800 -0.209 0.977 0.029; 800 -0.013 0.536 0.844; 800 -0.57 0.336 0.75; 800 0.722 0.642 -0.256; 800 -0.936 0.207 -0.286; 800 -0.137 0.396 -0.908; 800 0.64 0.691 0.335; 800 -0.207 0.824 -0.527; 800 -0.776 -0.184 0.604; 800 -0.475 0.762 0.44; 800 -0.904 0.326 0.279; 800 0.344 0.165 0.924; 800 0.971 0.241 -0.005; 800 0.292 0.944 -0.157; 800 -0.27 -0.053 0.961; 800 -0.687 0.71 -0.154; 800 0.169 0.861 0.479; 800 0.352 0.623 -0.699; 800 -0.623 0.375 -0.686; 800 0.749 0.261 0.609; 0 0 0 0];
% %     btable=[0 0 0 0; 800 -0.209 0.977 0.029; 800 -0.013 0.536 0.844; 800 -0.57 0.336 0.75; 800 0.722 0.642 -0.256; 800 -0.936 0.207 -0.286; 800 -0.137 0.396 -0.908; 800 0.64 0.691 0.335; 800 -0.207 0.824 -0.527; 800 -0.776 -0.184 0.604; 800 -0.475 0.762 0.44; 800 -0.904 0.326 0.279; 800 0.344 0.165 0.924; 800 0.971 0.241 -0.005; 800 0.292 0.944 -0.157; 800 -0.27 -0.053 0.961; 800 -0.687 0.71 -0.154; 800 0.169 0.861 0.479; 800 0.352 0.623 -0.699; 800 -0.623 0.375 -0.686; 800 0.749 0.261 0.609];
% %     btable=flip(btable);
% else
%     disp('Error: Need to pull b-table')
% end
fname=[dirOutput, 'BValVec.txt'];
if exist(fname)
    btable=readmatrix(fname);
    if size(btable,1)~=size(sDWI,4)
        disp('Error: The B-table from the BValVec.txt file does not match the dimensions of the DWI data.')
    end
else
    disp('Error: A BValVec.txt file needs to be created for this patient.')
end
% writematrix(btable,fname,'Delimiter','tab');

% sDWI=zeros(rowDWI,columnDWI,sliceDWI,nd);
nsub=rowDWIlarge/rowDWI;
gyro = 42.57; % kHz/mT
% 
% for i=1:nd
%    strfile=[dirPatient,dirDWI,'\',snDWI,num2str(i),'.dcm'];
%    mosaic=double(dicomread(strfile));
%    di=dicominfo(strfile);
%    
%    TE(i)=di.EchoTime*1e-3;
%    Bvalue(i)=di.Private_0019_100c*1e-6;
%    Gnorm(i)=sqrt(Bvalue(i)/((2*pi*gyro*delta(i))^2*(Delta(i)-delta(i)/3)));
%    if isfield(di,'Private_0019_100e')
%        Gx(i)=di.Private_0019_100e(1);
%        Gy(i)=di.Private_0019_100e(2);
%        Gz(i)=di.Private_0019_100e(3);
%    end
%    
%    for j=1:sliceDWI
%        x=floor((double(j)-0.1)/double(nsub))*rowDWI+1;
%        y=mod(j,nsub);
%        if y==0
%            y=nsub;
%        end
%        y=(y-1)*columnDWI+1;
%        sDWI(:,:,j,i)=mosaic(x:x+rowDWI-1,y:y+columnDWI-1);
%    end
% end
% 
% %% save b0 image and the DTI mask
% nex=0;
% b0=zeros(rowDWI,columnDWI,sliceDWI);
% for i=1:nd
%     if Bvalue(i)==0 && Gx(i)==0 && Gy(i)==0 && Gz(i)==0
%         b0=b0+sDWI(:,:,:,i);
%         nex=nex+1;
%     end
% end
% b0=b0/nex;
% maskDWI=zeros(rowDWI,columnDWI,sliceDWI);
maskDWI=zeros(size(b0));
maskDWI(b0>=10)=1;%%%% noise level

mat2analyze_file(b0,[dirOutput,strSite,'final_DTI_B0.nii'],[dxDWI dyDWI dzDWI],16);

%this flip is for matching the DSIstudio data
mat2analyze_file(flip(maskDWI,2),[dirOutput,strSite,'dti_mask_flipped.nii'],[dxDWI dyDWI dzDWI],16);
mat2analyze_file(sDWI,[dirOutput,strSite,'final_DTI.nii'],[dxDWI dyDWI dzDWI],16);

%Eddy Current
if exist([dirRaw eddyname])
    img=vuOpenImage([dirRaw eddyname]);
    mat2analyze_file(sDWI,[dirOutput,strSite,'final_DTI_EDDY.nii'],[dxDWI dyDWI dzDWI],16);
end

%% save averaged folder, mosaic image
% dirDWIavg=[dirPatient,dirDWI,'_Averaged'];
% if 0 == exist(dirDWIavg,'dir')
%     mkdir(dirDWIavg) 
% end

%ndiff=nd/nex;

% for i=1:ndiff
%     di=dicominfo([dirPatient,dirDWI,'\',snDWI,num2str(i),'.dcm']);
%     data=zeros(di.Rows,di.Columns,nex);
%     for j=1:nex
%         data(:,:,j)=double(dicomread([dirPatient,dirDWI,'\',snDWI,num2str(i+(j-1)*ndiff),'.dcm']));
%     end
%     avg=mean(data,3);
%     dicomwrite(int16(avg),[dirDWIavg,'\',snDWI,num2str(i),'.dcm'],'WritePrivate', true,di);
% end

%% DSI Studio commandline processing
% dsi_studio='C:\dsi_studio_win\dsi_studio';
dsi_studio='/Applications/dsi_studio.app/Contents/MacOS/dsi_studio';

%convert dicom to src
dirOutputTerm=['/Users/alisonroth/\Dropbox\ \(Barrow\ Neurological\ Institute\)/Research/CMT1A-R21/Scan\ \Development/' pat '/Processed/'];
strsrc=[' --source=',dirOutputTerm,strSite,'final_DTI.nii'];
strout=[' --output=',dirOutputTerm,strSite,'dti.src.gz'];
btab=[' --b_table=',dirOutputTerm,'BValVec.txt'];
doscmd=[dsi_studio,' --action=src',strsrc,btab,strout];
unix(doscmd);

%dti fitting
strsrc=[' --source=',dirOutputTerm,strSite,'dti.src.gz'];
strsrceddy=[' --source=',dirOutputTerm,strSite,'final_DTI_EDDY.nii'];
strmask=[' --mask=',dirOutputTerm,strSite,'dti_mask_flipped.nii'];
strmethod=' --method=1';%dti
if exist([dirRaw eddyname])
    doscmd=[dsi_studio,' --action=rec',strsrc,strsrceddy,strmask,strmethod];
else
    doscmd=[dsi_studio,' --action=rec',strsrc,strmask,strmethod];
end
dos(doscmd);

%export maps
strsrc=[' --source=',dirOutputTerm,strSite,'dti.src.gz.dti.fib.gz'];
strout=' --export=fa,ad,rd,md';
doscmd=[dsi_studio, ' --action=exp',strsrc,strout];
dos(doscmd);

%% save final dti maps
strfa=gunzip([dirOutput,strSite,'dti.src.gz.dti.fib.gz.fa.nii.gz']);
strmd=gunzip([dirOutput,strSite,'dti.src.gz.dti.fib.gz.md.nii.gz']);
strad=gunzip([dirOutput,strSite,'dti.src.gz.dti.fib.gz.ad.nii.gz']);
strrd=gunzip([dirOutput,strSite,'dti.src.gz.dti.fib.gz.rd.nii.gz']);

%flip back for the final results
fa=flip(read_analyze(char(strfa)),2);
md=flip(read_analyze(char(strmd)),2);
ad=flip(read_analyze(char(strad)),2);
rd=flip(read_analyze(char(strrd)),2);

mat2analyze_file(fa.*maskDWI*1000,[dirOutput,strSite,'final_DTI_FA.nii'],[dxDWI dyDWI dzDWI],16);
mat2analyze_file(md.*maskDWI*1000,[dirOutput,strSite,'final_DTI_MD.nii'],[dxDWI dyDWI dzDWI],16);
mat2analyze_file(ad.*maskDWI*1000,[dirOutput,strSite,'final_DTI_AD.nii'],[dxDWI dyDWI dzDWI],16);
mat2analyze_file(rd.*maskDWI*1000,[dirOutput,strSite,'final_DTI_RD.nii'],[dxDWI dyDWI dzDWI],16);

%delete temporary files
delete([dirOutput,strSite,'dti*.*']);

