%Comparison of B1 methods
close all; clear; clc
pat = {'01','03'};
site={'thigh','calf'};

addpath('/Users/alisonroth/Documents/Code/MATLAB/qMT')
addpath('/Users/alisonroth/Documents/Code/MATLAB/colormaps')
load('jetb')

for p=1:size(pat,2)
    %load data
    folder=['/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/CMT1A-R21/Scan Development/dev_' pat{p} '/Processed/'];
    load([folder 'CMT_dev' pat{p} '_' site{p} '.mat']);
    afi=load_nii([folder site{p} 'final_afiB1plus.nii']);
    afi=afi.img/1000;
    dam=load_nii([folder site{p} 'final_damB1plus.nii']);
    dam=dam.img/1000;
    MTR=load_nii([folder site{p} 'final_MTRapp.nii']);
    mask=load_nii([folder site{p} 'final_Mask_fatsat.nii']);
    
    imagesc(myMontage([afi(:,:,2),dam(:,:,2)]))
    colormap(jetb)
    colorbar
    
    %Compare B1+ maps
    diff_afi_dam=double(afi-dam);
    imtool3D(diff_afi_dam)
    colormap(jetb)
    colorbar
    
    temp=cat(4,afi, dam);
    temp=mean(temp,4);
    per_diff_afi_dam=diff_afi_dam./temp;
    per_diff_afi_dam(isnan(per_diff_afi_dam))=0;
    
    
    mean_diff=mean(diff_afi_dam(mask.img>0))
    mean_per_diff=mean(per_diff_afi_dam(mask.img>0))
    
    
    ind=find(mask.img>0);
    [~,~,slice]=ind2sub(size(mask.img),ind);
    slice=slice/40;
    scatter(afi(mask.img>0),dam(mask.img>0),[],slice)
    title(['dev ' pat{p}]);
    xlabel('AFI')
    ylabel('DAM')
    c=colorbar('Ticks',[0:0.1:1],'TickLabels',40*[0:0.1:1]);
    c.Label.String = 'Slice';
    saveas(gcf,[folder 'Images/' site{p} '_b1scatter'], 'epsc');
    
    %% Compare effects on MTR correction
    mtr=double(MTR.img);

    % MTR correction using AFI
    B1Err=(double(afi)/1000-AFIFA*pi/180)/(AFIFA*pi/180);
    pp=polyfit(mtr(mask.img>0),B1Err(mask.img>0),1);
    k=pp(1)/pp(2);
    mtrc_afi=mtr./(1+k*B1Err);
    
    mtrc_afi=mtrc_afi.*double(mask.img);
    mtrc_afi(isnan(mtrc_afi))=0;
    mtrc_afi(mtrc_afi<0)=0;
    mtrc_afi(mtrc_afi>100)=100;
    
    %MTR correction using DAM
    B1Err=(double(dam)/1000-AFIFA*pi/180)/(AFIFA*pi/180);
    pp=polyfit(mtr(mask.img>0),B1Err(mask.img>0),1);
    k=pp(1)/pp(2);
    mtrc_dam=mtr./(1+k*B1Err);
    
    mtrc_dam=mtrc_dam.*double(mask.img);
    mtrc_dam(isnan(mtrc_dam))=0;
    mtrc_dam(mtrc_dam<0)=0;
    mtrc_dam(mtrc_dam>100)=100;
    
    scatter(mtrc_afi(mask.img>0),mtr(mask.img>0),[],slice)
    title(['dev ' pat{p}]);
    xlabel('mtrc_afi')
    ylabel('mtr')
    c=colorbar('Ticks',[0:0.1:1],'TickLabels',40*[0:0.1:1]);
    c.Label.String = 'Slice';
    saveas(gcf,[folder 'Images/' site{p} '_afi_mtr_scatter'], 'epsc');
    
    scatter(mtrc_dam(mask.img>0),mtr(mask.img>0),[],slice)
    title(['dev ' pat{p}]);
    xlabel('mtrc_dam')
    ylabel('mtr')
    c=colorbar('Ticks',[0:0.1:1],'TickLabels',40*[0:0.1:1]);
    c.Label.String = 'Slice';
    saveas(gcf,[folder 'Images/' site{p} '_dam_mtr_scatter'], 'epsc');
    
    scatter(mtrc_afi(mask.img>0),mtrc_dam(mask.img>0),[],slice)
    title(['dev ' pat{p}]);
    xlabel('mtrc_afi')
    ylabel('mtrc_dam')
    c=colorbar('Ticks',[0:0.1:1],'TickLabels',40*[0:0.1:1]);
    c.Label.String = 'Slice';
    saveas(gcf,[folder 'Images/' site{p} '_mtr_afi_dam_scatter'], 'epsc');
end