% Yongsheng Chen <ys.chen@wayne.edu>; YC
% tested on MATLAB R2016a and R2019b w/
% Image Processing Toolbox
% Statistics and Machine Learning Toolbox

% Log:
% 8-13-2021, YC: Create.
% 8-30-2021, AR: added B1 correction for MTR and MTsat
% 9-7-2021, YC: added the VFA R1 for calculating MTsat
% 9-7-2021, YC: revised the B1 correction for MTR
% 9-8-2021, YC: added the Ernst angle data for testing MTsat
% 9-16-2021, YC: added the MTsat processing seciton
%
%% 
clear;close all;clc;

dirOutput='C:\CMT_Project_Data\CMT0032_JB_09012021_Output\';
datafile=[dirOutput,'thigh_CMT0032_JB_09012021.mat'];
Registration=1; % 0 for no-coregistration
wfalgo=2; % water fat decomposition algorithm: 1=hIDEAL; 2=QPBO

% function CMT_Data_Processing(dirOutput,datafile,Registration,wfalgo)

%% load data

% download the SPM12 and install, or add it to your search path
% or turn off image alignment by Registration=0;
addpath('..\spm12');

% load the datafile includes:
% UHR:      the ultra-high resolution gradient echo image
%           dxUHR, dyUHR, dzUHR are the voxel size; sUHR(row, column, slice) is
%           the magnitude image;
%
% MT:       Magnetization transfer on and off data
%           dxMT, dyMT, dzMT, MTOFFTEs, sMTON(row,column,slice,echo), and sMTOFF();
%
%           //09072021
%           PDW and T1W w/ fatsat are stored in sMTpdw() and sMTt1W();
%
%           //09082021
%           The Ernst angle data for test is stored in sMTernst();
%
%           The first two echoes will be avaged for MTR and MTsat
%
% GRE:      Gradient echo data for T1 mapping
%           dxGRE, dyGRE, dzGRE, GRETR, GREFAs, sPDW, and sT1W
%
% AFI:      Actual Flip Angle interleaved scan for B1 mapping
%           dxAFI, dyAFI, dzAFI, sAFI(row,column,slice,TR), AFIFA, and AFITRs;
%
% mDixon:   Multi-echo gradient echo data for f/w decomposition
%           dxmDixon, dymDixon, dzmDixon, mDixonTEs, smDixon(row,column,slice,echo),
%           and the phase smDixonp();
load(datafile);

%% AFI B1 mapping
minB1=0.3;
maxB1=2.0;
r=sAFI(:,:,:,2)./sAFI(:,:,:,1);
n=AFITRs(2)/AFITRs(1);
aFA=abs(acos((n.*r-1)./(n-r)));
AFIB1map=aFA./(AFIFA.*pi./180);
AFIB1map(isnan(AFIB1map))=0;
AFIB1map(AFIB1map<minB1)=0;
AFIB1map(AFIB1map>maxB1)=0;

%% mDixon f/w decomp. using Hierarchical IDEAL or QPBO
    dimmDixon=size(smDixon);

    compim=smDixon.*exp(1i*smDixonp);
    images=single(zeros(dimmDixon(1),dimmDixon(2),dimmDixon(3),1,dimmDixon(4)));
    for e=1:dimmDixon(4)
        images(:,:,:,1,e)=single(compim(:,:,:,e));
    end
    imDataParams.images=images;
    imDataParams.TE=mDixonTEs/1000.0;%ms ot s
    imDataParams.FieldStrength=3.0;%3T
    imDataParams.PrecessionIsClockwise=1;

    clear algoParams;
    algoParams.species(1).name = 'Water';
    algoParams.species(1).frequency = 4.7;
    algoParams.species(1).relAmps = 1;
    algoParams.species(2).name = 'Fat (6 peaks)';
    algoParams.species(2).frequency = [0.9000    1.3000    2.1000    2.7600    4.3100    5.3000];  % ppm
    algoParams.species(2).relAmps =   [0.0871    0.6937    0.1281    0.0040    0.0390    0.0480];

% if wfalgo==1 %hIDEAL
    % hIDEAL f/w decomposition from ISMRM f/w toolbox 2012
    addpath tsao_jiang/;
    
    algoParams.MinFractSizeToDivide = 0.01;
    algoParams.MaxNumDiv = 7;
    outParams = fw_i2cm0c_3pluspoint_tsaojiang(imDataParams, algoParams);
    
% else %berglund QPBO
%     % this is downloaded from:
%     % https://github.com/welcheb/FattyRiot
%     % the ISRMM 2012 toolbox version has a bug in DixonApp.m, which is
%     % corrected in below:
%     % https://github.com/welcheb/fw_i3cm1i_3pluspoint_berglund_QPBO
%     
%     algoParams.decoupled_estimation = true; % flag for decoupled R2 estimation
%     algoParams.Fibonacci_search = true; % flag for Fibonacci search
%     algoParams.B0_smooth_in_stack_direction = false; % flag for B0 smooth in stack direction
%     algoParams.multigrid = true; % flag for multi-level resolution pyramid
%     algoParams.estimate_R2 = true; % flag to estimate R2star
%     algoParams.verbose = true; % flag for verbose status messages (default false)
%     algoParams.process_in_3D = false; % flag to process in 3D (default true)
%     algoParams.R2star_calibration = true; % flag to perform R2* calibration (default false)
%     algoParams.ICM_iterations = 2; % ICM iterations
%     algoParams.num_B0_labels = 100; % number of discretized B0 values
%     algoParams.mu = 10; % regularization parameter
%     algoParams.R2_stepsize = 1; % R2 stepsize in s^-1
%     algoParams.max_R2 = 120; % maximum R2 in s^-1
%     algoParams.max_label_change = 0.1; % 
%     algoParams.fine_R2_stepsize = 1.0; % Fine stepsize for discretization of R2(*) [s^-1] (used in decoupled fine-tuning step)
%     algoParams.coarse_R2_stepsize = 10.0; % Coarse stepsize for discretization of R2(*) [s^-1] (used for joint estimation step, value 0.0 --> Decoupled estimation only
%     algoParams.water_R2 = 0.0; % Water R2 [sec-1]
%     algoParams.fat_R2s = zeros(1,9); % fat peak R2s [s^-1]
%     algoParams.R2star_calibration_max = 800; % max R2* to use for calibration [s^-1] (default 800)
%     algoParams.R2star_calibration_cdf_threshold = 0.98; %% threshold for R2* calibration cumulative density function [0,1] (default 0.98)
% 
%     outParams = fw_i3cm1i_3pluspoint_berglund_QPBO(imDataParams,algoParams);
% end

mDixonW = abs(outParams.species(1).amps);
mDixonF = abs(outParams.species(2).amps);

mDixonFW=zeros(dimmDixon(1),dimmDixon(2),dimmDixon(3)*2);
mDixonFW(:,:,[1:dimmDixon(3)])=mDixonF;
mDixonFW(:,:,[1:dimmDixon(3)]+dimmDixon(3))=mDixonW;

mDixonFW = mDixonFW - min(mDixonFW(:));
mDixonFW = mDixonFW / max(mDixonFW(:));
mDixonF = mDixonFW(:,:,[1:dimmDixon(3)]);
mDixonW = mDixonFW(:,:,[1:dimmDixon(3)]+dimmDixon(3));
mDixonFF = mDixonF./(mDixonW+mDixonF)*100;
mDixonFF(isnan(mDixonFF))=0;

%% R2star, T2star from MTOFF
minT2s=5; %5 ms as the minimum t2*
maxT2s=200; %200 ms as the maximum t2*
maxR2s=1/minT2s;
minR2s=1/maxT2s;

[R2sMTOFF, ~] = STAGE_R2star(sMTOFF, MTOFFTEs, length(MTOFFTEs), ones(size(sMTOFF(:,:,:,1))));

T2sMTOFF=1./R2sMTOFF;

T2sMTOFF(isnan(T2sMTOFF))=0;
T2sMTOFF(T2sMTOFF<minT2s)=0;
T2sMTOFF(T2sMTOFF>maxT2s)=0;

R2sMTOFF(isnan(R2sMTOFF))=0;
R2sMTOFF(R2sMTOFF<minR2s)=0;
R2sMTOFF(R2sMTOFF>maxR2s)=0;

%% image resigtration

if Registration >0 %SPM12 registration
    sUHRgre=imresize(sUHR,dxUHR/dxGRE);
    sUHRmt=imresize(sUHR,dxUHR/dxMT);
    sUHRmDixon=imresize(sUHR,dxUHR/dxmDixon);
    sUHRafi=imresize(sUHR,dxUHR/dxAFI);

    mat2analyze_file(sUHR,[dirOutput,strSite,'UHR.nii'],[dxUHR dyUHR dzUHR],16);
    mat2analyze_file(sUHRgre,[dirOutput,strSite,'UHRgresize.nii'],[dxGRE dyGRE dzGRE],16);
    mat2analyze_file(sUHRmt,[dirOutput,strSite,'UHRmtsize.nii'],[dxMT dyMT dzMT],16);
    mat2analyze_file(sUHRmDixon,[dirOutput,strSite,'UHRmDixonsize.nii'],[dxmDixon dymDixon dzmDixon],16);
    mat2analyze_file(sUHRafi,[dirOutput,strSite,'UHRafisize.nii'],[dxAFI dyAFI dzAFI],16);

    prefix='regToUHR_';

    mat2analyze_file(smDixon(:,:,:,2),[dirOutput,strSite,'mDixonIP.nii'],[dxmDixon dymDixon dzmDixon],16);
    mat2analyze_file(smDixon(:,:,:,4),[dirOutput,strSite,'mDixonOP.nii'],[dxmDixon dymDixon dzmDixon],16);
    mat2analyze_file(mDixonW*1000,[dirOutput,strSite,'mDixonW.nii'],[dxmDixon dymDixon dzmDixon],16);
    mat2analyze_file(mDixonF*1000,[dirOutput,strSite,'mDixonF.nii'],[dxmDixon dymDixon dzmDixon],16);
    mat2analyze_file(mDixonFF,[dirOutput,strSite,'mDixonFF.nii'],[dxmDixon dymDixon dzmDixon],16);
    
    mat2analyze_file(sPDW,[dirOutput,strSite,'PDW.nii'],[dxGRE dyGRE dzGRE],16);
    mat2analyze_file(sT1W,[dirOutput,strSite,'T1W.nii'],[dxGRE dyGRE dzGRE],16);
    
    mat2analyze_file(R2sMTOFF*1000,[dirOutput,strSite,'R2sMTOFF.nii'],[dxMT dyMT dzMT],16);
    mat2analyze_file(T2sMTOFF,[dirOutput,strSite,'T2sMTOFF.nii'],[dxMT dyMT dzMT],16);
    
    %09072021, YC, average first 2 echoes of MT
    mat2analyze_file(mean(sMTON(:,:,:,1:2),4),[dirOutput,strSite,'MTON.nii'],[dxMT dyMT dzMT],16);
    mat2analyze_file(mean(sMTOFF(:,:,:,1:2),4),[dirOutput,strSite,'MTOFF.nii'],[dxMT dyMT dzMT],16);
    
    %09072021, YC, pdw and t1w for MTsat; average
    mat2analyze_file(mean(sMTpdw(:,:,:,:),4),[dirOutput,strSite,'MTpdw.nii'],[dxMT dyMT dzMT],16);
    mat2analyze_file(mean(sMTt1w(:,:,:,:),4),[dirOutput,strSite,'MTt1w.nii'],[dxMT dyMT dzMT],16);

    %09082021, YC, the Ernst angle for testing MTsat; average
    mat2analyze_file(mean(sMTernst(:,:,:,:),4),[dirOutput,strSite,'MTernst.nii'],[dxMT dyMT dzMT],16);

    mat2analyze_file(sAFI(:,:,:,1),[dirOutput,strSite,'AFITR1.nii'],[dxAFI dyAFI dzAFI],16);
    mat2analyze_file(AFIB1map*1000,[dirOutput,strSite,'AFIB1map.nii'],[dxAFI dyAFI dzAFI],16);
    
    %reg PDW to UHR
    ref_nii_path={[dirOutput,strSite,'UHRgresize.nii',',1']};
    mov_nii_path={[dirOutput,strSite,'PDW.nii',',1']};
    other_nii_path={[]};
    SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)

    %reg T1W to UHR
    ref_nii_path={[dirOutput,strSite,'UHRgresize.nii',',1']};
    mov_nii_path={[dirOutput,strSite,'T1W.nii',',1']};
    other_nii_path={[]};
    SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)

    %reg mDixon to UHR
    ref_nii_path={[dirOutput,strSite,'UHRmDixonsize.nii',',1']};
    mov_nii_path={[dirOutput,strSite,'mDixonIP.nii',',1']};
    other_nii_path={
        [dirOutput,strSite,'mDixonOP.nii',',1']
        [dirOutput,strSite,'mDixonW.nii',',1']
        [dirOutput,strSite,'mDixonF.nii',',1']
        [dirOutput,strSite,'mDixonFF.nii',',1']
        };
    SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)
    
    %reg MTON to UHR
    ref_nii_path={[dirOutput,strSite,'UHRmtsize.nii',',1']};
    mov_nii_path={[dirOutput,strSite,'MTON.nii',',1']};
    other_nii_path={[]};
    SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)

    %reg MTOFF to UHR
    ref_nii_path={[dirOutput,strSite,'UHRmtsize.nii',',1']};
    mov_nii_path={[dirOutput,strSite,'MTOFF.nii',',1']};
    other_nii_path={
        [dirOutput,strSite,'R2sMTOFF.nii',',1']
        [dirOutput,strSite,'T2sMTOFF.nii',',1']
        };
    SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)

    %reg MTpdw to UHR
    ref_nii_path={[dirOutput,strSite,'UHRmtsize.nii',',1']};
    mov_nii_path={[dirOutput,strSite,'MTpdw.nii',',1']};
    other_nii_path={[]};
    SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)

    %reg MTt1w to UHR
    ref_nii_path={[dirOutput,strSite,'UHRmtsize.nii',',1']};
    mov_nii_path={[dirOutput,strSite,'MTt1w.nii',',1']};
    other_nii_path={[]};
    SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)

    %reg MTernst to UHR
    ref_nii_path={[dirOutput,strSite,'UHRmtsize.nii',',1']};
    mov_nii_path={[dirOutput,strSite,'MTernst.nii',',1']};
    other_nii_path={[]};
    SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)

    %reg AFI to UHR
    ref_nii_path={[dirOutput,strSite,'UHRafisize.nii',',1']};
    mov_nii_path={[dirOutput,strSite,'AFITR1.nii',',1']};
    other_nii_path={
        [dirOutput,strSite,'AFIB1map.nii',',1']
        };
    SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)

    %load reged data for further processing  
    reged_PDW=read_analyze([dirOutput,prefix,strSite,'PDW.nii']);
    reged_T1W=read_analyze([dirOutput,prefix,strSite,'T1W.nii']);
    
    reged_mDixonIP=read_analyze([dirOutput,prefix,strSite,'mDixonIP.nii']);
    reged_mDixonOP=read_analyze([dirOutput,prefix,strSite,'mDixonOP.nii']);
    reged_mDixonW=read_analyze([dirOutput,prefix,strSite,'mDixonW.nii']);
    reged_mDixonF=read_analyze([dirOutput,prefix,strSite,'mDixonF.nii']);
    reged_mDixonFF=read_analyze([dirOutput,prefix,strSite,'mDixonFF.nii']);
    
    reged_R2sMTOFF=read_analyze([dirOutput,prefix,strSite,'R2sMTOFF.nii']);
    reged_T2sMTOFF=read_analyze([dirOutput,prefix,strSite,'T2sMTOFF.nii']);
    reged_MTON=read_analyze([dirOutput,prefix,strSite,'MTON.nii']);
    reged_MTOFF=read_analyze([dirOutput,prefix,strSite,'MTOFF.nii']);
    reged_MTpdw=read_analyze([dirOutput,prefix,strSite,'MTpdw.nii']);
    reged_MTt1w=read_analyze([dirOutput,prefix,strSite,'MTt1w.nii']);
    reged_MTernst=read_analyze([dirOutput,prefix,strSite,'MTernst.nii']);
    
    reged_AFIB1map=read_analyze([dirOutput,prefix,strSite,'AFIB1map.nii'])/1000;
    
    delete([dirOutput,strSite,'UHR.nii']);
    delete([dirOutput,strSite,'UHRgresize.nii']);
    delete([dirOutput,strSite,'UHRmtsize.nii']);
    delete([dirOutput,strSite,'UHRmDixonsize.nii']);
    delete([dirOutput,strSite,'UHRafisize.nii']);
    
    delete([dirOutput,strSite,'mDixonIP.nii']);
    delete([dirOutput,strSite,'mDixonOP.nii']);
    delete([dirOutput,strSite,'mDixonW.nii']);
    delete([dirOutput,strSite,'mDixonF.nii']);
    delete([dirOutput,strSite,'mDixonFF.nii']);
    
    delete([dirOutput,strSite,'PDW.nii']);
    delete([dirOutput,strSite,'T1W.nii']);
    
    delete([dirOutput,strSite,'R2sMTOFF.nii']);
    delete([dirOutput,strSite,'T2sMTOFF.nii']);
    delete([dirOutput,strSite,'MTON.nii']);
    delete([dirOutput,strSite,'MTOFF.nii']);
    delete([dirOutput,strSite,'MTpdw.nii']);
    delete([dirOutput,strSite,'MTt1w.nii']);
    delete([dirOutput,strSite,'MTernst.nii']);
    
    delete([dirOutput,strSite,'AFITR1.nii']);
    delete([dirOutput,strSite,'AFIB1map.nii']);
    
    delete([dirOutput,prefix,strSite,'*.nii']);

else
    
    %no coregistration, original data
    reged_PDW=sPDW;
    reged_T1W=sT1W;
    
    reged_mDixonIP=smDixon(:,:,:,2);
    reged_mDixonOP=smDixon(:,:,:,4);
    reged_mDixonW=mDixonW*1000;
    reged_mDixonF=mDixonF*1000;
    reged_mDixonFF=mDixonFF;
    
    reged_R2sMTOFF=R2sMTOFF*1000;
    reged_T2sMTOFF=T2sMTOFF;
    reged_MTON=mean(sMTON(:,:,:,1:2),4);
    reged_MTOFF=mean(sMTOFF(:,:,:,1:2),4);
    reged_MTpdw=mean(sMTpdw(:,:,:,:),4);
    reged_MTt1w=mean(sMTt1w(:,:,:,:),4);
    reged_MTernst=mean(sMTernst(:,:,:,:),4);
    
    reged_AFIB1map=AFIB1map;
end

reged_PDW(isnan(reged_PDW))=0;
reged_T1W(isnan(reged_T1W))=0;

reged_mDixonIP(isnan(reged_mDixonIP))=0;
reged_mDixonOP(isnan(reged_mDixonOP))=0;
reged_mDixonW(isnan(reged_mDixonW))=0;
reged_mDixonF(isnan(reged_mDixonF))=0;
reged_mDixonFF(isnan(reged_mDixonFF))=0;

reged_R2sMTOFF(isnan(reged_R2sMTOFF))=0;
reged_T2sMTOFF(isnan(reged_T2sMTOFF))=0;
reged_MTON(isnan(reged_MTON))=0;
reged_MTOFF(isnan(reged_MTOFF))=0;
reged_MTpdw(isnan(reged_MTpdw))=0;
reged_MTt1w(isnan(reged_MTt1w))=0;
reged_MTernst(isnan(reged_MTernst))=0;

reged_AFIB1map(isnan(reged_AFIB1map))=0;

%% Mask
sNoise=35; %magnitude noise threshold
dimMT=size(reged_MTOFF);
Mask_fatsat=ones(dimMT(1),dimMT(2),dimMT(3));
Mask_fatsat(reged_MTOFF<sNoise)=0;% mask removing noise region

dimGRE=size(reged_PDW);
Mask_nofatsat=ones(dimGRE(1),dimGRE(2),dimGRE(3));
Mask_nofatsat(reged_PDW<sNoise)=0;% mask removing noise region

%% T1 mapping for the GRE data without FatSat
magGRE=zeros(dimGRE(1),dimGRE(2),dimGRE(3),2);
magGRE(:,:,:,1)=reged_PDW;
magGRE(:,:,:,2)=reged_T1W;
afi_B1map=imresize(reged_AFIB1map,[dimGRE(1:2)]);
afi_B1map(isnan(afi_B1map))=0;
k=ones(dimGRE(1),dimGRE(2),dimGRE(3),2);
k(:,:,:,1)=afi_B1map;
k(:,:,:,2)=afi_B1map;

[T1,p0,rsq]=fn_vfa_t1mapping(magGRE,GREFAs,k,GRETR,20000,20000,1);

T1(Mask_nofatsat<1)=0;
p0(Mask_nofatsat<1)=0;
T1(isnan(T1))=0;
p0(isnan(p0))=0;
mat2analyze_file(T1,[dirOutput,strSite,'final_T1.nii'],[dxGRE dyGRE dzGRE],16);
mat2analyze_file(p0,[dirOutput,strSite,'final_p0.nii'],[dxGRE dyGRE dzGRE],16);
mat2analyze_file(afi_B1map.*Mask_nofatsat.*1000,[dirOutput,strSite,'final_B1plus.nii'],[dxGRE dyGRE dzGRE],16);
mat2analyze_file(Mask_nofatsat.*1000,[dirOutput,strSite,'final_Mask_nofatsat.nii'],[dxGRE dyGRE dzGRE],4);
mat2analyze_file(Mask_fatsat.*1000,[dirOutput,strSite,'final_Mask_fatsat.nii'],[dxGRE dyGRE dzGRE],4);

%% T1 mapping for the GRE data with FatSat
% 09072021, YC
magMT=zeros(dimMT(1),dimMT(2),dimMT(3),2);
if exist('reged_MTpdw')
    magMT(:,:,:,1)=reged_MTpdw;%already averaged the two echoes
    MTsat_FAs=[MTpdwFA,MTt1wFA];
else
    magMT(:,:,:,1)=reged_MTOFF;%already averaged the two echoes
    MTsat_FAs=[MTFA,MTt1wFA];
    MTpdwTR=MTTR;
end
MTsat_TR=MTpdwTR;
magMT(:,:,:,2)=reged_MTt1w;
afi_B1mapMT=imresize(reged_AFIB1map,[dimMT(1:2)]);
afi_B1mapMT(isnan(afi_B1mapMT))=0;
k=ones(size(magMT));
k(:,:,:,1)=afi_B1mapMT;
k(:,:,:,2)=afi_B1mapMT;

[T1mt,p0mt,rsq]=fn_vfa_t1mapping(magMT,MTsat_FAs,k,MTsat_TR,20000,20000,1);

T1mt(Mask_fatsat<1)=0;
p0mt(Mask_fatsat<1)=0;
T1mt(isnan(T1mt))=0;
p0mt(isnan(p0mt))=0;
mat2analyze_file(T1mt,[dirOutput,strSite,'final_T1mt.nii'],[dxMT dyMT dzMT],16);
mat2analyze_file(p0mt,[dirOutput,strSite,'final_p0mt.nii'],[dxMT dyMT dzMT],16);

%% MTR and B1 corrected MTR

mtr=reged_MTON./reged_MTOFF;
mtr(isnan(mtr))=0;
mtr(mtr>2)=0;
mtr(mtr<0.1)=0;
mtr=(1-mtr)*100; %should we *100 here before the polyfit for the B1 correction?
mtr(isnan(mtr))=0;

% B1 corrected MTR from Alison Roth, 08302021
% B1Err=(afi_B1mapMT-AFIFA*pi/180)/(AFIFA*pi/180); %AR 08302021
alpha=AFIFA*pi/180; B1Err=(afi_B1mapMT*alpha-alpha)/alpha; %YC 09072021
p=polyfit(mtr(Mask_fatsat>0),B1Err(Mask_fatsat>0),1);
k=p(1)/p(2);
mtrc=mtr./(1+k*B1Err);

mtr=mtr.*Mask_fatsat;
mtrc=mtrc.*Mask_fatsat;

mtr(isnan(mtr))=0;
mtr(mtr<0)=0;
mtr(mtr>100)=100;

mtrc(isnan(mtrc))=0;
mtrc(mtrc<0)=0;
mtrc(mtrc>100)=100;

mat2analyze_file(mtr,[dirOutput,strSite,'final_MTRapp.nii'],[dxMT dyMT dzMT],16);
mat2analyze_file(mtrc,[dirOutput,strSite,'final_MTRcorr.nii'],[dxMT dyMT dzMT],16);

%% MTsat, YC, 09162021
% mrm.21732, Eq (8), here for MTsat, the R1 (R1app) and PD (Aapp) are from the VFA method
% which is the same as those in the Eq (7a, 7b) for the approximation
% method.
% Eq (8): delta=(Aapp*alpha/Smt-1)*R1app*TR-alpha^2/2  or for our code:
% mtsat = (Aapp*alpha/reged_MTON-1)*R1app*TRmt-alpha^2/2;

alpha=10/180*pi;%radians, flip angle of the MTOn scan
TRmt=40;%ms, TR of the MTOn scan
maxT1=20000;
minT1=100;
maxPD=20000;

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MTsat using the VFA R1
% three scans: MTRON (TR=40;FA=10), T1W (TR=20;FA=22), and PDW (TR=20;FA=5)
kmt=ones(size(magMT));
[T1app,Aapp,rsq]=fn_vfa_t1mapping(magMT,MTsat_FAs,kmt,MTsat_TR,maxT1,maxPD,1);
R1app=1000./T1app;
R1app(R1app>1000/minT1)=0;
R1app(R1app<1000/maxT1)=0;

mtsat = (Aapp.*alpha./reged_MTON-1).*R1app.*TRmt-alpha.^2./2;

mtsat=mtsat.*Mask_fatsat;
mtsat(isnan(mtsat))=0;
mtsat(mtsat<0)=0;
mtsat(mtsat>100)=100;

mat2analyze_file(mtsat,[dirOutput,strSite,'final_MTsat.nii'],[dxMT dyMT dzMT],16);
% mat2analyze_file(T1app.*Mask_fatsat,[dirOutput,strSite,'final_MTt1app.nii'],[dxMT dyMT dzMT],16);
% mat2analyze_file(Aapp.*Mask_fatsat,[dirOutput,strSite,'final_MTpdapp.nii'],[dxMT dyMT dzMT],16);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MTsat using the approximation R1 (Eq7 of the mrm.21732)
% three scans: MTRON (TR=40;FA=10), T1W (TR=20;FA=22), and PDW (TR=20;FA=5)
% this matsat2 should be our final MTsat using two additional scans to
% maximize the T1 precision
alphaPD=MTsat_FAs(1)/180*pi;
alphaT1=MTsat_FAs(2)/180*pi;
R1app2=1000.*0.5.*(reged_MTt1w.*alphaT1./MTt1wTR-reged_MTpdw.*alphaPD./MTpdwTR)./(reged_MTpdw./alphaPD-reged_MTt1w./alphaT1);
R1app2(R1app2>1000/minT1)=0;
R1app2(R1app2<1000/maxT1)=0;
T1app2=1000./R1app2;
T1app2(T1app2>maxT1)=0;
T1app2(T1app2<minT1)=0;
Aapp2=reged_MTpdw.*reged_MTt1w.*(MTsat_TR.*alphaT1./alphaPD-MTsat_TR.*alphaPD./alphaT1)./(reged_MTt1w.*MTsat_TR.*alphaT1-reged_MTpdw.*MTsat_TR.*alphaPD);
Aapp2(Aapp2>maxPD)=0;
Aapp2(Aapp2<0)=0;

mtsat2 = (Aapp2.*alpha./reged_MTON-1).*R1app2.*TRmt-alpha.^2./2;

mtsat2=mtsat2.*Mask_fatsat;
mtsat2(isnan(mtsat2))=0;
mtsat2(mtsat2<0)=0;
mtsat2(mtsat2>100)=100;

mat2analyze_file(mtsat2,[dirOutput,strSite,'final_MTsat2.nii'],[dxMT dyMT dzMT],16);
% mat2analyze_file(T1app2.*Mask_fatsat,[dirOutput,strSite,'final_MTt1app2.nii'],[dxMT dyMT dzMT],16);
% mat2analyze_file(Aapp2.*Mask_fatsat,[dirOutput,strSite,'final_MTpdapp2.nii'],[dxMT dyMT dzMT],16);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MTsat using the approximation R1 (Eq7 of the mrm.21732)
% three scans: MTRON (TR=40;FA=10), MTOFF (TR=40;FA=10), T1W (TR=20;FA=22) 
% here the PDW is the MTOFF scan, somehow PD weighted
R1app3=1000.*0.5.*(reged_MTt1w.*alphaT1./MTt1wTR-reged_MTOFF.*alpha./MTpdwTR)./(reged_MTOFF./alpha-reged_MTt1w./alphaT1);
R1app3(R1app3>1000/minT1)=0;
R1app3(R1app3<1000/maxT1)=0;
T1app3=1000./R1app3;
T1app3(T1app3>maxT1)=0;
T1app3(T1app3<minT1)=0;
Aapp3=reged_MTOFF.*reged_MTt1w.*(TRmt.*alphaT1./alpha-MTt1wTR.*alpha./alphaT1)./(reged_MTt1w.*TRmt.*alphaT1-reged_MTOFF.*MTt1wTR.*alpha);
Aapp3(Aapp3>maxPD)=0;
Aapp3(Aapp3<0)=0;

mtsat3 = (Aapp3.*alpha./reged_MTON-1).*R1app3.*TRmt-alpha.^2./2;

mtsat3=mtsat3.*Mask_fatsat;
mtsat3(isnan(mtsat3))=0;
mtsat3(mtsat3<0)=0;
mtsat3(mtsat3>100)=100;

mat2analyze_file(mtsat3,[dirOutput,strSite,'final_MTsat3.nii'],[dxMT dyMT dzMT],16);
% mat2analyze_file(T1app3.*Mask_fatsat,[dirOutput,strSite,'final_MTt1app3.nii'],[dxMT dyMT dzMT],16);
% mat2analyze_file(Aapp3.*Mask_fatsat,[dirOutput,strSite,'final_MTpdapp3.nii'],[dxMT dyMT dzMT],16);


%% save other files
mat2analyze_file(sUHR,[dirOutput,strSite,'final_UHR.nii'],[dxUHR dyUHR dzUHR],16);

mat2analyze_file(reged_PDW,[dirOutput,strSite,'final_PDW.nii'],[dxGRE dyGRE dzGRE],16);
mat2analyze_file(reged_T1W,[dirOutput,strSite,'final_T1W.nii'],[dxGRE dyGRE dzGRE],16);

mat2analyze_file(reged_MTOFF,[dirOutput,strSite,'final_MTOFF.nii'],[dxMT dyMT dzMT],16);
mat2analyze_file(reged_MTON,[dirOutput,strSite,'final_MTON.nii'],[dxMT dyMT dzMT],16);
mat2analyze_file(reged_MTpdw,[dirOutput,strSite,'final_MTpdw.nii'],[dxMT dyMT dzMT],16);
mat2analyze_file(reged_MTt1w,[dirOutput,strSite,'final_MTt1w.nii'],[dxMT dyMT dzMT],16);
mat2analyze_file(reged_MTernst,[dirOutput,strSite,'final_MTernst.nii'],[dxMT dyMT dzMT],16);

mat2analyze_file(reged_R2sMTOFF.*Mask_fatsat,[dirOutput,strSite,'final_R2s.nii'],[dxMT dyMT dzMT],16);
mat2analyze_file(reged_T2sMTOFF.*Mask_fatsat,[dirOutput,strSite,'final_T2s.nii'],[dxMT dyMT dzMT],16);

dimmDixon=size(reged_mDixonW);
maskmDixon=ones(dimmDixon(1),dimmDixon(2),dimmDixon(3));
maskmDixon(reged_mDixonIP<sNoise)=0;
mat2analyze_file(reged_mDixonIP,[dirOutput,strSite,'final_mDixonInPhaseImage.nii'],[dxmDixon dymDixon dzmDixon],16);
mat2analyze_file(reged_mDixonOP,[dirOutput,strSite,'final_mDixonOutOfPhaseImage.nii'],[dxmDixon dymDixon dzmDixon],16);
mat2analyze_file(reged_mDixonW,[dirOutput,strSite,'final_mDixonWaterImage.nii'],[dxmDixon dymDixon dzmDixon],16);
mat2analyze_file(reged_mDixonF,[dirOutput,strSite,'final_mDixonFatImage.nii'],[dxmDixon dymDixon dzmDixon],16);
mat2analyze_file(reged_mDixonFF.*maskmDixon,[dirOutput,strSite,'final_mDixonFatFraction.nii'],[dxmDixon dymDixon dzmDixon],16);

%%
return; 


