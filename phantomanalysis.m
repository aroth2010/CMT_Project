close all; clear; clc;
fold='/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/CMT1A-R21/Scan Development/phantom_03/Processed/';
% load([fold 'CMT_phantom_03_thigh.mat']);
ff=load_nii([fold 'thighfinal_mDixonFatFraction.nii']);
fat=load_nii([fold 'thighfinal_mDixonFatImage.nii']);
water=load_nii([fold 'thighfinal_mDixonWaterImage.nii']);
seg=load_nii([fold 'Segmentation-label.nii']);

fat=fat.img;
water=water.img;
ff=ff.img;
seg=seg.img;

seg_ff=imresize(seg,size(ff,1)/size(seg,1));

% x=zeros(128*128,1);
% y=zeros(128*128,1);
% z=zeros(128*128,1);
% k=1;
% for i=1:size(seg_ff,1)
%     for j=1:size(seg_ff,1)
%         x(k)=i;
%         y(k)=j;
%         z(k)=10;
%     end
% end
% ind0=sub2ind(size(seg_ff),x,y,z);

seg_ff=seg_ff(:,:,10);
ff=ff(:,:,10);
mn=zeros(8,1);
stadev=zeros(8,1);
for i=1:8
    ind=find(seg_ff==i);
    mn(i)=mean(ff(ind));
    stadev(i)=std(ff(ind));
end

ff2=100*fat./(fat+water);
ff2=ff2(:,:,10);
mn2=zeros(8,1);
stadev2=zeros(8,1);
for i=1:8
    ind=find(seg_ff==i);
    mn2(i)=mean(ff(ind));
    stadev2(i)=std(ff(ind));
end