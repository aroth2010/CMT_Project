clear;close all;clc;
pat='05';

addpath('/Users/alisonroth/Documents/Code/MATLAB/vuTools/')

% % dirOutput='C:\CMT_Project_Data\CMT0011_JH_07132021_Output\'; %Yongsheng
dirOutput=['/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/CMT1A-R21/Scan Development/Phantom_' pat '/Processed/']; %Alison %dev_ %Change to registered scans!
% % datafile=[dirOutput,'thigh_CMT0011_JH_07132021.mat']; %Yongsheng
strSite='thigh';
mapfile=[dirOutput,'CMT_dev_' pat '_' strSite '_maps.mat']; %Alison %CMT_dev
savefilename=[dirOutput,'CMT_dev_' pat '_' strSite '_stats.mat'];

%function CMT_Data_Analysis(dirOutput,datafile,Registration,strSite)
%% Before running this code, segment the relevant tissues

%% load data
load(mapfile);
ROI=load_nii([dirOutput strSite '_ROI.nii']);
ROI.img=read_analyze([dirOutput strSite '_ROI.nii']);

%Resize ROI to fat fraction, MTR/MTsat
if size(ROI.img,1)~=size(mtr,1)
    scale=size(mtr,1)/size(ROI.img,1);
    ROImt=imresize(ROI.img,scale,'nearest');
else
    ROImt=ROI.img;
end

if size(ROI.img,1)~=size(reged_mDixonFF,1)
    scale=size(reged_mDixonFF,1)/size(ROI.img,1);
    ROIff=imresize(ROI.img,scale,'nearest');
else
    ROIff=ROI.img;
end

%Find unique tissues/segmentations
tissues=unique(ROI.img(:));
tissues=tissues(2:end);

%Find stats: Tissue/Segementation #, Volume, Fat Fraction Mean, Fat Fraction StDev,MTR Mean,MTR StDev,MTsat Mean, MTsat StDev
stats=zeros(size(tissues,1),11); %Add DTI metrics
for t=tissues'
    stats(t,1)=t;
    ind=find(ROI.img==t);
    stats(t,2)=size(ind,1)*ROI.hdr.hist.qoffset_x*ROI.hdr.hist.qoffset_y*ROI.hdr.hist.qoffset_z; %mm^3

    %Fat Fraction
    ind=find(ROIff==t);
    stats(t,3)=mean(reged_mDixonFF(ind));
    stats(t,4)=median(reged_mDixonFF(ind));
    stats(t,5)=std(reged_mDixonFF(ind));

    %MTR
    ind=find(ROImt==t);
    stats(t,6)=mean(mtr(ind));
    stats(t,7)=median(mtr(ind));
    stats(t,8)=std(mtr(ind));

    %MTsat
    stats(t,9)=mean(mtsat(ind));
    stats(t,10)=median(mtsat(ind));
    stats(t,11)=std(mtsat(ind));
end
stats=array2table(stats,"VariableNames",{'Tissue','Volume [mm^3]','Fat Fraction Mean', 'Fat Fraction Median','Fat Fraction StDev','MTR Mean','MTR Median','MTR StDev','MTsat Mean','MTsat Median', 'MTsat StDev'});
save(savefilename,"stats");

cd('/Users/alisonroth/Documents/Code/MATLAB/CMT_Project/');

tempff=(ROIff>0).*reged_mDixonFF;
tempmtr=(ROImt>0).*mtr;
tempmtsat=(ROImt>0).*mtsat;
% tempuhr=(ROI.img>0).*sUHR;

if ~exist([dirOutput 'Images'])
    mkdir([dirOutput 'Images'])
end
slice=size(reged_mDixonFF,3)/2;

figure('Position',[10000,10000,500,500])
imagesc(reged_mDixonFF(:,:,slice),[0,30]), axis image off, xticklabels(''); yticklabels(''); colorbar; colormap("jet")
set(gca,'LooseInset',get(gca,'TightInset'));
fname=[dirOutput 'Images/FF_10.png'];
saveas(gcf,fname);

imagesc(tempmtr(:,:,slice),[0,100]), axis image off, xticklabels(''); yticklabels(''); colorbar; colormap("jet")
set(gca,'LooseInset',get(gca,'TightInset'));
fname=[dirOutput 'Images/MTR_10.png'];
saveas(gcf,fname);

imagesc(mtsat(:,:,slice),[0,5]), axis image off, xticklabels(''); yticklabels(''); colorbar; colormap("jet")
set(gca,'LooseInset',get(gca,'TightInset'));
fname=[dirOutput 'Images/MTsat_10.png'];
saveas(gcf,fname);

% imagesc(tempuhr(:,:,slice),[0,20]), axis image off, xticklabels(''); yticklabels(''); colorbar; colormap("jet")
% set(gca,'LooseInset',get(gca,'TightInset'));
% fname=[dirOutput 'Images/UHR_10.png'];
% saveas(gcf,fname);
% close all;

% end