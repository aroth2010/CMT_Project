clear all, clc; %close all
%set purpose!
%preprocess runs initial preprocessing before registration
%check: Check registration
%roi: Create ROIs
%analysis: finishes pre-processing & runs MT analysis codes
purpose='analysis';

% Set directories
analysisDIR = '/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/qMT/';
niiDIR = [analysisDIR 'niiData/'];
addpath('/Users/alisonroth/Documents/Code/MATLAB/vuTools/');
addpath('/Users/alisonroth/Documents/Code/MATLAB/Data_Load_Save/');
addpath('/Users/alisonroth/Documents/Code/MATLAB/imaging-features/');
addpath(genpath('/Users/alisonroth/Documents/Code/MATLAB/qMRLab-2.4.1/'));

% Load study information from Excel file
studyInfoFileName = [analysisDIR 'studyInfo_qMTSciaticNerve.xlsx'];
[num,txt,raw] = xlsread(studyInfoFileName, 'Data');

ind=1;
subjectID = raw{ind+1,1};
disp(['Starting Subject: ' num2str(subjectID*10) ' ' datestr(datetime)])

regDIR=[niiDIR 'Subject_' num2str(subjectID*10) '/'];
fnameREG = [regDIR 'regData_' num2str(subjectID*10) '.mat'];
load(fnameREG);
    
    %MTw
    MTFA=6;
    if subjectID == 1001
        MTTR = 52/1000; %ms
    else
        MTTR = 55/1000; %ms
    end
    
    %PDw
    PDFA=MTFA;
    PDTR=MTTR;
    
    %T1w
    T1FA=30;
    T1TR=25/1000;
    
    [MTsat,~,~,uMTsat,~,~]=MTsatCalc(regDIR, subjectID,qMTr(:,:,:,end),PDFA,PDTR,qMTr(:,:,:,1),MTFA,MTTR,MFAr(:,:,:,5),T1FA,T1TR,ROI,B1s,MASK);
    
    
    %Yongsheng's methods:
    MTsat_FAs=[MTFA T1FA];
    alpha=MTFA*pi()/180;
    alphaPD=MTFA*pi()/180;
    alphaT1=T1FA*pi()/180;
    Mask_fatsat=MASK;
    MTpdwTR=MTTR;
    MTsat_TR=MTTR*1000;
    MTt1wTR=T1TR;
    reged_MTOFF=qMT(:,:,:,end);
    reged_MTON=qMT(:,:,:,1);
    reged_MTpdw=qMT(:,:,:,end);
    reged_MTt1w=MFA(:,:,:,end);
    TRmt=MTTR;
    magMT=qMT(:,:,:,end);
    magMT=cat(4,magMT,MFA(:,:,:,5));    
    
    
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MTsat using the VFA R1
% three scans: MTRON (TR=40;FA=10), T1W (TR=20;FA=22), and PDW (TR=20;FA=5)
maxT1=20000;
minT1=100;
maxPD=20000;
kmt=ones(size(magMT));
[T1app,Aapp,rsq]=fn_vfa_t1mapping(magMT,MTsat_FAs,kmt,MTsat_TR,maxT1,maxPD,1);
R1app=1000./T1app;
R1app(R1app>1000/minT1)=0;
R1app(R1app<1000/maxT1)=0;

mtsat = (Aapp.*alpha./reged_MTON-1).*R1app.*TRmt-alpha.^2./2;

mtsat=mtsat.*Mask_fatsat;
mtsat(isnan(mtsat))=0;
mtsat(mtsat<0)=0;
mtsat(mtsat>100)=100;

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MTsat using the approximation R1 (Eq7 of the mrm.21732)
% three scans: MTRON (TR=40;FA=10), MTOFF (TR=40;FA=10), T1W (TR=20;FA=22) 
% here the PDW is the MTOFF scan, somehow PD weighted
R1app3=1000.*0.5.*(reged_MTt1w.*alphaT1./MTt1wTR-reged_MTOFF.*alpha./MTpdwTR)./(reged_MTOFF./alpha-reged_MTt1w./alphaT1);
R1app3(R1app3>1000/minT1)=0;
R1app3(R1app3<1000/maxT1)=0;
T1app3=1000./R1app3;
T1app3(T1app3>maxT1)=0;
T1app3(T1app3<minT1)=0;
Aapp3=reged_MTOFF.*reged_MTt1w.*(TRmt.*alphaT1./alpha-MTt1wTR.*alpha./alphaT1)./(reged_MTt1w.*TRmt.*alphaT1-reged_MTOFF.*MTt1wTR.*alpha);
Aapp3(Aapp3>maxPD)=0;
Aapp3(Aapp3<0)=0;

mtsat3 = (Aapp3.*alpha./reged_MTON-1).*R1app3.*TRmt-alpha.^2./2;

mtsat3=mtsat3.*Mask_fatsat;
mtsat3(isnan(mtsat3))=0;
mtsat3(mtsat3<0)=0;
mtsat3(mtsat3>100)=100;
    
    
    [u,s,~,~]=roiStatsVolume(mtsat3,ROI);
    uMTsat3=mean(u);

    
    
    imagesc(myMontage([MTsat(:,:,10),mtsat(:,:,10),mtsat3(:,:,10)]),[0,1]); colorbar
    